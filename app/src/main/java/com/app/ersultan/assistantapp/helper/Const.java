package com.app.ersultan.assistantapp.helper;

public class Const {

    public static final String DOMAIN_URL = "http://asst.kz";
    public static final String BASE_URL = "http://asst.kz/api/v1/";
    public static final String MAIN_URL = BASE_URL + "main";
    public static final String GET_CATEGORIES = BASE_URL + "categories";
    public static final String GET_VENUE_BY_ID = BASE_URL + "venue?venue_id=";
    public static final String URL_LOGIN = BASE_URL + "login";
    public static final String URL_ADD_FCM_TOKEN = BASE_URL + "push/add";
    public static final String URL_DELETE_FCM_TOKEN = BASE_URL + "push/delete";
    public static final String URL_DELETE_USER = BASE_URL + "user/delete";
    public static final String URL_ADD_REVIEW = BASE_URL + "review/add";
    public static final String URL_GET_REVIEWS = BASE_URL + "reviews?venue_id=";
    public static final String URL_GET_VENUES_BY_CATEGORY_ID = BASE_URL + "main/venues?menu_id=";
    public static final String URL_GET_VENUES_BY_NAVIGATION_DRAWER_CATEGORY_ID = BASE_URL + "venues?category_id=";
    public static final String URL_GET_ALL_NOTIFICATIONS = BASE_URL + "notifications";
    public static final String URL_GET_NOTIFICATION_BY_ID = BASE_URL + "notification?notification_id=";
    public static final String URL_MAIN_PAGE_SEARCH = BASE_URL + "search?search_text=";

    public static final String accessToken = "accessToken";

    public static final String venueId = "venueId";
    public static final String venueType = "venueType";
    public static final String venueLatitude = "venueLatitude";
    public static final String venueLongitude = "venueLongitude";
    public static final String venueAddress = "venueAddress";
    public static final String agreementPage = "agreementPage";
    public static final String venueName = "venueName";
    public static final String menuPrice = "menuPrice";
    public static final String menuDiscount = "menuDiscount";
    public static final String menuServiceCharge = "menuServiceCharge";
    public static final String reviewsCount = "reviewsCount";
    public static final String venuesCategoryId = "venuesCategoryId";
    public static final String venuesCategoryName = "venuesCategoryName";
    public static final String filterTitle = "filterTitle";
    public static final String filterType = "filterType";
    public static final String filterArray = "filterArray";
    public static final String filterModel = "filterModel";
    public static final String informationPosition = "informationPosition";
    public static final String otherFilterPosition = "otherFilterPosition";
    public static final String layoutPosition = "layoutPosition";
    public static final String notificationId = "notificationId";
    public static final String fromNotificationActivity = "fromNotificationActivity";

    public static final String checkedRegionFilterItems = "checkedRegionFilterItems";
    public static final String checkedWorkTimeFilterItems = "checkedWorkTimeFilterItems";
    public static final String checkedOtherFilterItems = "checkedOtherFilterItems";
    public static final String checkedPriceFilterItems = "checkedPriceFilterItems";

    public static final String regionType = "regionType";
    public static final String workTimeType = "workRegionType";
    public static final String otherFilterType = "otherFilterType";
}
