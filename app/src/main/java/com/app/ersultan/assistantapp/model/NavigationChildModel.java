package com.app.ersultan.assistantapp.model;

/**
 * Created by ersultan on 11/2/17.
 */

public class NavigationChildModel {

    private String childTitle = "";
    private String childCount = "";
    private String childId = "";

    public NavigationChildModel(){}

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getChildTitle() {
        return childTitle;
    }

    public void setChildTitle(String childTitle) {
        this.childTitle = childTitle;
    }

    public String getChildCount() {
        return childCount;
    }

    public void setChildCount(String childCount) {
        this.childCount = childCount;
    }

}

