package com.app.ersultan.assistantapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by ersultan on 11/9/17.
 */

public class MenuGroupModel {

    private String menuGroupTitle = "";
    private String menuGroupId = "";

    public MenuGroupModel(){}

    protected MenuGroupModel(Parcel in) {
        menuGroupTitle = in.readString();
        menuGroupId = in.readString();
    }

    public String getMenuGroupTitle() {
        return menuGroupTitle;
    }

    public void setMenuGroupTitle(String menuGroupTitle) {
        this.menuGroupTitle = menuGroupTitle;
    }

    public String getMenuGroupId() {
        return menuGroupId;
    }

    public void setMenuGroupId(String menuGroupId) {
        this.menuGroupId = menuGroupId;
    }

}
