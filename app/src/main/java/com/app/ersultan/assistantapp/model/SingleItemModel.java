package com.app.ersultan.assistantapp.model;

import java.util.List;

/**
 * Created by ersultan on 9/28/17.
 */

public class SingleItemModel {

    private String id;
    private String type;
    private String name;
    private String image;
    private String rating;
    private String reviews;
    private String discount;

    public SingleItemModel(){}

//    public SingleItemModel(String venueId, String image, String reviews, String rating, String type, String name){
//        this.id = venueId;
//        this.reviews = reviews;
//        this.rating = rating;
//        this.type = type;
//        this.name = name;
//        this.image = image;
//    }


    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL() {
        return image;
    }

    public void setImageURL(String image) {
        this.image = image;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }
}
