package com.app.ersultan.assistantapp.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.model.PriceChildModel;
import com.app.ersultan.assistantapp.model.PriceGroupModel;
import com.app.ersultan.assistantapp.widget.ExpandableItemIndicator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ersultan on 12/4/17.
 */

public class ExpandablePriceAdapter extends AbstractExpandableItemAdapter<ExpandablePriceAdapter.MyGroupViewHolder, ExpandablePriceAdapter.MyChildViewHolder>{

    public static final String TAG = ExpandablePriceAdapter.class.getSimpleName();
    private RecyclerViewExpandableItemManager mExpandableManager;
    private Context mContext;
    private List<Pair<PriceGroupModel, List<PriceChildModel>>> data = new LinkedList<>();

    // NOTE: Make accessible with short name
    private interface Expandable extends ExpandableItemConstants { }

    public static abstract class MyBaseViewHolder extends AbstractExpandableItemViewHolder {
        public FrameLayout mContainer;

        public MyBaseViewHolder(View v) {
            super(v);
            mContainer = v.findViewById(R.id.container);
        }
    }

    public static class MyGroupViewHolder extends ExpandablePriceAdapter.MyBaseViewHolder{
        public ExpandableItemIndicator mIndicator;
        public TextView priceGroupTitle;

        public MyGroupViewHolder(View view){
            super(view);
            mIndicator = view.findViewById(R.id.indicator_price);
            priceGroupTitle = view.findViewById(R.id.text_price_group_title);
        }
    }

    public static class MyChildViewHolder extends ExpandablePriceAdapter.MyBaseViewHolder{
        public TextView priceChildTitle, priceChildPriceValue;
        public ImageView ivCheck;
        public RelativeLayout rlChild;

        public MyChildViewHolder(View view){
            super(view);
            priceChildTitle = view.findViewById(R.id.text_price_child_title);
            priceChildPriceValue = view.findViewById(R.id.text_price_child_price_value);
            rlChild = view.findViewById(R.id.rlPriceChildContent);
            ivCheck = view.findViewById(R.id.image_view_check);
        }
    }

    public ExpandablePriceAdapter(Context context, RecyclerViewExpandableItemManager expMgr, List<Pair<PriceGroupModel, List<PriceChildModel>>> data) {
        this.mContext = context;
        this.data = data;
        this.mExpandableManager = expMgr;
        setHasStableIds(true);
    }

    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return data.get(groupPosition).second.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return Integer.parseInt(data.get(groupPosition).first.getPriceGroupId());
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return Integer.parseInt(data.get(groupPosition).second.get(childPosition).getPriceChildId());
    }

    @Override
    public int getGroupItemViewType(int groupPosition) {
        return 0;
    }

    @Override
    public int getChildItemViewType(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public MyGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.list_price_group_item, parent, false);
        return new MyGroupViewHolder(view);
    }

    @Override
    public MyChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.list_price_child_item, parent, false);
        return new MyChildViewHolder(view);
    }

    @Override
    public void onBindGroupViewHolder(MyGroupViewHolder holder, int groupPosition, int viewType) {
        // group item
        final PriceGroupModel priceGroupModel = data.get(groupPosition).first;
        holder.priceGroupTitle.setText(priceGroupModel.getPriceGroupTitle());
        // mark as clickable
        holder.itemView.setClickable(true);
        // set background resource (target view ID: container)
        final int expandState = holder.getExpandStateFlags();

        if ((expandState & ExpandableItemConstants.STATE_FLAG_IS_UPDATED) != 0) {
            boolean isExpanded;
            boolean animateIndicator = ((expandState & Expandable.STATE_FLAG_HAS_EXPANDED_STATE_CHANGED) != 0);

            if ((expandState & Expandable.STATE_FLAG_IS_EXPANDED) != 0) {
                isExpanded = true;
            } else {
                isExpanded = false;
            }

            holder.mIndicator.setExpandedState(isExpanded, animateIndicator);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindChildViewHolder(final MyChildViewHolder holder, final int groupPosition, final int childPosition, int viewType) {
        //child item
        final PriceChildModel childModel = data.get(groupPosition).second.get(childPosition);

        holder.priceChildTitle.setText(childModel.getPriceChildTitle());

        if(childModel.getPriceChildValue().equals("") || childModel.getPriceChildValue().equals("-1") || childModel.getPriceChildValue().equals(mContext.getString(R.string.price_with_symbol))){
            holder.priceChildPriceValue.setText(mContext.getString(R.string.price_with_symbol));
        } else {
            String value = childModel.getPriceChildValue().replaceAll(" тг.", "");
            holder.priceChildPriceValue.setText(value + " тг.");
        }

        if(childModel.getChecked().equals("-1")){
            holder.ivCheck.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_not_checked));
        } else if(childModel.getChecked().equals("1")){
            holder.ivCheck.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_checked));
        }

        holder.itemView.setClickable(true);
        holder.priceChildPriceValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(20, 20, 20, 10);

                LinearLayout linearLayout = new LinearLayout(mContext);
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                linearLayout.setLayoutParams(layoutParams);

                InputFilter[] FilterArray = new InputFilter[1];
                FilterArray[0] = new InputFilter.LengthFilter(7);

                final EditText editText = new EditText(mContext);
                editText.setLayoutParams(layoutParams);
                editText.setFilters(FilterArray);
                editText.setHint(mContext.getString(R.string.price_with_symbol));

                Log.i(TAG, "CHILD PRICE VALUE: " + childModel.getPriceChildValue());

                if(!childModel.getPriceChildValue().equals("") && !childModel.getPriceChildValue().equals(mContext.getString(R.string.price_with_symbol)) && !childModel.getPriceChildValue().equals("-1")){
                    String withoutCurrency = childModel.getPriceChildValue();
                    withoutCurrency = withoutCurrency.replaceAll(" тг.", "");
                    editText.setText(withoutCurrency);
                } else {
                    editText.setText("");
                }

                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                editText.setBackground(ContextCompat.getDrawable(mContext, R.drawable.select_price_edit_text));
                editText.setPadding(10, 10, 10, 10);
                linearLayout.addView(editText);

                alert.setTitle(mContext.getString(R.string.specify_own_price));
                alert.setView(linearLayout);

                alert.setPositiveButton("Ок", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(!editText.getText().toString().isEmpty()){
                            data.get(groupPosition).second.get(childPosition).setPriceChildValue(editText.getText().toString()  + " тг.");

                            holder.priceChildPriceValue.setText(editText.getText().toString() + " тг.");

                            Log.i(TAG, "ADAPTER CHILD VALUE: " + holder.priceChildPriceValue.getText().toString());

                            holder.ivCheck.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_checked));
                            data.get(groupPosition).second.get(childPosition).setChecked("1");

                        } else {
                            holder.priceChildPriceValue.setText(mContext.getString(R.string.price_with_symbol));
                            data.get(groupPosition).second.get(childPosition).setPriceChildValue("");
                        }
                    }
                });

                alert.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                alert.show();
            }
        });

        holder.rlChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(data.get(groupPosition).second.get(childPosition).getChecked().equals("-1")){
                    holder.ivCheck.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_checked));
                    data.get(groupPosition).second.get(childPosition).setChecked("1");

                } else {
                    holder.ivCheck.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_not_checked));
                    data.get(groupPosition).second.get(childPosition).setChecked("-1");
                }
            }
        });

        if(childModel.getChildPosition().equals("first or middle")){
            holder.rlChild.setBackground(ContextCompat.getDrawable(mContext, R.drawable.first_or_middle_menu_item));
        } else if(childModel.getChildPosition().equals("last")){
            holder.rlChild.setBackground(ContextCompat.getDrawable(mContext, R.drawable.last_menu_item));
        }
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(MyGroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        return true;
    }

}















