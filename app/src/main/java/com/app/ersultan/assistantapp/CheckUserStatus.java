package com.app.ersultan.assistantapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.app.ersultan.assistantapp.activity.AuthorizationActivity;
import com.app.ersultan.assistantapp.activity.StartWorkActivity;
import com.app.ersultan.assistantapp.helper.Preferences;

/**
 * Created by ersultan on 9/25/17.
 */

public class CheckUserStatus extends Activity{

    @Override
    protected void onStart() {
        super.onStart();
        setVisible(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startActivity(new Intent(CheckUserStatus.this, StartWorkActivity.class)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));

//        if(!logged && !firstVisiting)
//            startActivity(new Intent(CheckUserStatus.this, StartWorkActivity.class)
//                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                    .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
//        else if(!logged && firstVisiting)
//            startActivity(new Intent(CheckUserStatus.this, AuthorizationActivity.class)
//                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                    .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
//        else if(logged)
//            startActivity(new Intent(CheckUserStatus.this, MainActivity.class)
//                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                    .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
    }

}
