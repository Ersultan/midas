package com.app.ersultan.assistantapp.adapter;

/**
 * Created by ersultan on 11/18/17.
 */

public interface OnListItemClickMessageListener {
    void onItemClicked(String message);
}
