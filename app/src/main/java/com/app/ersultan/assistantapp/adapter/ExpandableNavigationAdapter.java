package com.app.ersultan.assistantapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.util.Pair;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.activity.NotificationsActivity;
import com.app.ersultan.assistantapp.activity.VenueWithFiltersActivity;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.model.NavigationChildModel;
import com.app.ersultan.assistantapp.model.NavigationGroupModel;
import com.app.ersultan.assistantapp.widget.ExpandableItemIndicator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ersultan on 11/2/17.
 */

public class ExpandableNavigationAdapter extends AbstractExpandableItemAdapter<ExpandableNavigationAdapter.MyGroupViewHolder, ExpandableNavigationAdapter.MyChildViewHolder> {
    public static final String TAG = ExpandableNavigationAdapter.class.getSimpleName();
    private RecyclerViewExpandableItemManager mExpandableManager;
    private Context mContext;
    private DrawerLayout drawerLayout;
    private List<Pair<NavigationGroupModel, List<NavigationChildModel>>> data = new LinkedList<>();

    // NOTE: Make accessible with short name
    private interface Expandable extends ExpandableItemConstants { }

    public static abstract class MyBaseViewHolder extends AbstractExpandableItemViewHolder {
        public FrameLayout mContainer;

        public MyBaseViewHolder(View v) {
            super(v);
            mContainer = v.findViewById(R.id.container);
        }
    }

    public static class MyGroupViewHolder extends MyBaseViewHolder {
        public ExpandableItemIndicator mIndicator;
        public TextView categoryName;
        public ImageView categoryIcon;

        public MyGroupViewHolder(View v) {
            super(v);
            mIndicator = v.findViewById(R.id.indicator);
            categoryName = v.findViewById(R.id.venueTypeName);
            categoryIcon = v.findViewById(R.id.venueTypeNameImage);
        }
    }

    public static class MyChildViewHolder extends MyBaseViewHolder {
        public TextView venueType, venuesCount;

        public MyChildViewHolder(View v) {
            super(v);
            venueType = v.findViewById(R.id.venueTypes);
            venuesCount = v.findViewById(R.id.venuesCount);
        }
    }

    public ExpandableNavigationAdapter(Context context, RecyclerViewExpandableItemManager expandableManager, List<Pair<NavigationGroupModel, List<NavigationChildModel>>> data, DrawerLayout drawerLayout) {
        this.mContext = context;
        this.drawerLayout = drawerLayout;
        this.data = data;
        this.mExpandableManager = expandableManager;
        this.mExpandableManager.setOnGroupExpandListener(new RecyclerViewExpandableItemManager.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition, boolean fromUser, Object payload) {
                for(int i = 0; i < mExpandableManager.getGroupCount(); i++) {
                    if(groupPosition != i) {
                        mExpandableManager.collapseGroup(i);
                    }
                }
            }
        });
        // ExpandableItemAdapter requires stable ID, and also
        // have to implement the getGroupItemId()/getChildItemId() methods appropriately.
        setHasStableIds(true);
    }

    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return data.get(groupPosition).second.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return Integer.parseInt(data.get(groupPosition).first.getGroupId());
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return Integer.parseInt(data.get(groupPosition).second.get(childPosition).getChildId());
    }

    @Override
    public int getGroupItemViewType(int groupPosition) {
        return 0;
    }

    @Override
    public int getChildItemViewType(int groupPosition, int childPosition) {
        return 0;
    }

        @Override
        public MyGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final View v = inflater.inflate(R.layout.list_navigation_group_item, parent, false);
            return new MyGroupViewHolder(v);
        }

        @Override
        public MyChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final View v = inflater.inflate(R.layout.list_navigation_child_item, parent, false);

            return new MyChildViewHolder(v);
        }

    @Override
    public void onBindGroupViewHolder(final MyGroupViewHolder holder, int groupPosition, int viewType) {
        // child item
        final NavigationGroupModel groupItem = data.get(groupPosition).first;

        // set text
        holder.categoryName.setText(groupItem.getGroupTitle());
        holder.categoryIcon.setBackgroundResource(groupItem.getGroupIcon());

        if(groupItem.getGroupTitle().equals(mContext.getString(R.string.notifications))){
            holder.mIndicator.setVisibility(View.GONE);
        } else {
            holder.mIndicator.setVisibility(View.VISIBLE);
        }

        // mark as clickable
        holder.itemView.setClickable(true);

        // set background resource (target view ID: container)
        final int expandState = holder.getExpandStateFlags();

        if ((expandState & ExpandableItemConstants.STATE_FLAG_IS_UPDATED) != 0) {
            boolean isExpanded;
            boolean animateIndicator = ((expandState & Expandable.STATE_FLAG_HAS_EXPANDED_STATE_CHANGED) != 0);

            if ((expandState & Expandable.STATE_FLAG_IS_EXPANDED) != 0) {
                isExpanded = true;
            } else {
                isExpanded = false;
            }

            holder.mIndicator.setExpandedState(isExpanded, animateIndicator);
        }

        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(groupItem.getGroupTitle().equals(mContext.getString(R.string.notifications))){
                    drawerLayout.closeDrawers();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mContext.startActivity(new Intent(mContext, NotificationsActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        }
                    }, 250);
                }
            }
        });
    }

    @Override
    public void onBindChildViewHolder(final MyChildViewHolder holder, int groupPosition, int childPosition, int viewType) {
        // child item
        final NavigationChildModel item = data.get(groupPosition).second.get(childPosition);

        // set text
        holder.venueType.setText(item.getChildTitle());
        holder.venuesCount.setText(item.getChildCount());

        holder.itemView.setClickable(true);
        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mContext.startActivity(new Intent(mContext, VenueWithFiltersActivity.class)
                                .putExtra(Const.venueType, item.getChildTitle())
                                .putExtra(Const.venuesCategoryId, item.getChildId())
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    }
                }, 250);
            }
        });

    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(MyGroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        return true;
    }

}
