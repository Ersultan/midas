package com.app.ersultan.assistantapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.adapter.FilterVariantsAdapter;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.helper.RecyclerTouchListener;
import com.app.ersultan.assistantapp.model.DistrictModel;
import com.app.ersultan.assistantapp.model.FilterVariantsModel;
import com.app.ersultan.assistantapp.model.FiltersModel;
import com.app.ersultan.assistantapp.model.ValuesOfFiltersModel;
import com.app.ersultan.assistantapp.model.WorkTimeModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;

public class ChooseFilterVariantsActivity extends BaseActivity {

    public static final String TAG = ChooseFilterVariantsActivity.class.getSimpleName();
    public ArrayList<DistrictModel> districtModelArrayList = new ArrayList<>();
    public ArrayList<WorkTimeModel> workTimeModelArrayList = new ArrayList<>();
    public ArrayList<ValuesOfFiltersModel> valuesOfFiltersModelArrayList = new ArrayList<>();

    @BindView(R.id.recycler_view_filter_items)
    RecyclerView recyclerView;
    private List<FilterVariantsModel> filterVariantsModelList = new ArrayList<>();
    private FilterVariantsAdapter adapter;

    private String checkedRegionsId = "";
    private String checkedWorkTimeId = "";

    private String checkedOtherFiltersId = "";
    private String notCheckedOtherFiltersId = "";

    private int currentOtherFilterListPosition = 0;
    private int layoutPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_filter_variants);
        setToolbar();

        Intent intent = getIntent();
        if(intent.getStringExtra(Const.filterType).equals(Const.regionType)){
            districtModelArrayList = (ArrayList<DistrictModel>) intent.getSerializableExtra(Const.filterArray);
        } else if(intent.getStringExtra(Const.filterType).equals(Const.workTimeType)){
            workTimeModelArrayList = (ArrayList<WorkTimeModel>) intent.getSerializableExtra(Const.filterArray);
        } else if(intent.getStringExtra(Const.filterType).equals(Const.otherFilterType)){
            valuesOfFiltersModelArrayList = (ArrayList<ValuesOfFiltersModel>) intent.getSerializableExtra(Const.filterArray);
            currentOtherFilterListPosition = intent.getIntExtra(Const.otherFilterPosition, 0);
            layoutPosition = intent.getIntExtra(Const.layoutPosition, 0);
        }

        prepareRecyclerView();
        showFilter();
    }

    private void setCheckedRegionsId(){
        Intent intent = getIntent();
        for(int i = 0; i < districtModelArrayList.size(); i++){
            if(districtModelArrayList.get(i).getDistrictId().equals(intent.getStringExtra(Const.checkedRegionFilterItems))){
                filterVariantsModelList.get(i).setFilterCheckIcon(R.drawable.check_mark_button);
            }
        }
    }

    private void setCheckedWorkTimeId(){
        Intent intent = getIntent();
        for(int i = 0; i < workTimeModelArrayList.size(); i++){
            if(workTimeModelArrayList.get(i).getTimeId().equals(intent.getStringExtra(Const.checkedWorkTimeFilterItems))){
                filterVariantsModelList.get(i).setFilterCheckIcon(R.drawable.check_mark_button);
            }
        }
    }

    private void setOtherFiltersCheckedItems(){
        Intent intent = getIntent();
        Log.i(TAG, "OTHER FILTERS CHECKED ID: " + intent.getStringExtra(Const.checkedOtherFilterItems));

        if(!intent.getStringExtra(Const.checkedOtherFilterItems).equals("")){
            String[] checkedOtherFilterItemsArray = intent.getStringExtra(Const.checkedOtherFilterItems).split(",");
            checkedOtherFiltersId = intent.getStringExtra(Const.checkedOtherFilterItems);
            for(int i = 0; i < valuesOfFiltersModelArrayList.size(); i++){
                for(int j = 0; j < checkedOtherFilterItemsArray.length; j++){
                    if(valuesOfFiltersModelArrayList.get(i).getValueId().equals(checkedOtherFilterItemsArray[j])){
                        filterVariantsModelList.get(i).setFilterCheckIcon(R.drawable.check_mark_button);
                    }
                }
            }
        }
    }

    private void prepareRecyclerView(){
        adapter = new FilterVariantsAdapter(ChooseFilterVariantsActivity.this, filterVariantsModelList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ChooseFilterVariantsActivity.this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(ChooseFilterVariantsActivity.this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = getIntent();
                if(intent.getStringExtra(Const.filterType).equals(Const.regionType)){
                    if(filterVariantsModelList.get(position).getFilterCheckIcon() != R.drawable.check_mark_button){
                        filterVariantsModelList.get(position).setFilterCheckIcon(R.drawable.check_mark_button);
                    } else {
                        filterVariantsModelList.get(position).setFilterCheckIcon(0);
                    }

                    for(int i = 0; i < filterVariantsModelList.size(); i++){
                        if(i != position){
                            filterVariantsModelList.get(i).setFilterCheckIcon(0);
                        }
                    }

                } else if(intent.getStringExtra(Const.filterType).equals(Const.workTimeType)){
                    if(filterVariantsModelList.get(position).getFilterCheckIcon() != R.drawable.check_mark_button){
                        filterVariantsModelList.get(position).setFilterCheckIcon(R.drawable.check_mark_button);
                    } else {
                        filterVariantsModelList.get(position).setFilterCheckIcon(0);
                    }

                    for(int i = 0; i < filterVariantsModelList.size(); i++){
                        if(i != position){
                            filterVariantsModelList.get(i).setFilterCheckIcon(0);
                        }
                    }
                } else if(intent.getStringExtra(Const.filterType).equals(Const.otherFilterType)){
                    if(filterVariantsModelList.get(position).getFilterCheckIcon() != R.drawable.check_mark_button){
                        filterVariantsModelList.get(position).setFilterCheckIcon(R.drawable.check_mark_button);
                    } else {
                        filterVariantsModelList.get(position).setFilterCheckIcon(0);
                    }
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void showFilter(){
        Intent intent = getIntent();
        if(intent.getStringExtra(Const.filterType).equals(Const.regionType)){
            for(int i = 0; i < districtModelArrayList.size(); i++){
                FilterVariantsModel filterVariantsModel = new FilterVariantsModel();
                filterVariantsModel.setFilterTitle(districtModelArrayList.get(i).getDistrictTitle());
                filterVariantsModelList.add(filterVariantsModel);
            }

            setCheckedRegionsId();
        } else if(intent.getStringExtra(Const.filterType).equals(Const.workTimeType)){
            for(int i = 0; i < workTimeModelArrayList.size(); i++){
                FilterVariantsModel filterVariantsModel = new FilterVariantsModel();
                filterVariantsModel.setFilterTitle(workTimeModelArrayList.get(i).getTime());
                filterVariantsModelList.add(filterVariantsModel);
            }

            setCheckedWorkTimeId();
        } else if(intent.getStringExtra(Const.filterType).equals(Const.otherFilterType)){
            for(int i = 0; i < valuesOfFiltersModelArrayList.size(); i++){
                FilterVariantsModel filterVariantsModel = new FilterVariantsModel();
                filterVariantsModel.setFilterTitle(valuesOfFiltersModelArrayList.get(i).getValueTitle());
                filterVariantsModelList.add(filterVariantsModel);
            }

            setOtherFiltersCheckedItems();
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getSearchMenuItem().setVisible(false);
        getFilterMenuItem().setVisible(false);
        getOkMenuItem().setVisible(true);
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_ok:
                Intent intent = new Intent();
                Intent returnedIntent = getIntent();

                if(returnedIntent.getStringExtra(Const.filterType).equals(Const.regionType)){
                    for(int i = 0; i < filterVariantsModelList.size(); i++){
                        if(filterVariantsModelList.get(i).getFilterCheckIcon() == R.drawable.check_mark_button){
                            checkedRegionsId += districtModelArrayList.get(i).getDistrictId();
                        }
                    }

                    intent.putExtra(Const.checkedRegionFilterItems, checkedRegionsId);

                } else if(returnedIntent.getStringExtra(Const.filterType).equals(Const.workTimeType)){
                    for(int i = 0; i < filterVariantsModelList.size(); i++){
                        if(filterVariantsModelList.get(i).getFilterCheckIcon() == R.drawable.check_mark_button){
                            checkedWorkTimeId += workTimeModelArrayList.get(i).getTimeId();
                        }
                    }

                    intent.putExtra(Const.checkedWorkTimeFilterItems, checkedWorkTimeId);

                } else if(returnedIntent.getStringExtra(Const.filterType).equals(Const.otherFilterType)){
                    // checked items
                    for(int i = 0; i < filterVariantsModelList.size(); i++){
                        if(filterVariantsModelList.get(i).getFilterCheckIcon() == R.drawable.check_mark_button){
                            checkedOtherFiltersId += valuesOfFiltersModelArrayList.get(i).getValueId() + ",";
                        }
                    }

                    // not checked items
                    for(int i = 0; i < filterVariantsModelList.size(); i++){
                        if(filterVariantsModelList.get(i).getFilterCheckIcon() != R.drawable.check_mark_button){
                            notCheckedOtherFiltersId += valuesOfFiltersModelArrayList.get(i).getValueId() + ",";
                        }
                    }

                    checkedOtherFiltersId = removeDuplicatesComma(checkedOtherFiltersId) + ",";

                    String[] notCheckedArray = notCheckedOtherFiltersId.split(",");
                    String[] checkedArray = checkedOtherFiltersId.split(",");
                    String newCheckedArray = "";

                    if(notCheckedArray.length > 0){
                        for(int i = 0; i < checkedArray.length; i++){
                            boolean contains = false;
                            for(int j = 0; j < notCheckedArray.length; j++){
                                Log.i(TAG, "I = " + String.valueOf(checkedArray[i]));
                                Log.i(TAG, "J = " + String.valueOf(notCheckedArray[j]));
                                if(String.valueOf(checkedArray[i]).equals(notCheckedArray[j])){
                                    Log.i(TAG, "CONTAINS TRUe: " + checkedArray[i]);
                                    contains = true;
                                }
                            }

                            if(!contains){
                                newCheckedArray += checkedArray[i] + ",";
                                Log.i(TAG, "NEW CHECKED CONTAINS: " + newCheckedArray);
                            }
                        }
                    }

                    checkedOtherFiltersId = newCheckedArray;

                    // remove duplicates
                    checkedOtherFiltersId = removeDuplicatesComma(checkedOtherFiltersId) + ",";

                    Log.i(TAG, "NOT CHECKED ITEMS ID: " + notCheckedOtherFiltersId);
                    Log.i(TAG, "CHECKED ITEMS: " + checkedOtherFiltersId);

                    intent.putExtra(Const.checkedOtherFilterItems, checkedOtherFiltersId);
                    intent.putExtra(Const.otherFilterPosition, currentOtherFilterListPosition);
                    intent.putExtra(Const.layoutPosition, layoutPosition);
                }

                setResult(RESULT_OK, intent);
                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String removeDuplicatesComma(String yourText) {
        Set<String> elements = new LinkedHashSet<>(Arrays.asList(yourText.split(",")));

        Iterator<String> it = elements.iterator();

        StringBuilder sb = new StringBuilder(it.hasNext() ? it.next() : "");
        while (it.hasNext()) {
            sb.append(',').append(it.next());
        }

        return sb.toString();
    }

    public void setToolbar(){
        Intent intent = getIntent();
        getTvToolbarTitle().setText(intent.getStringExtra(Const.filterTitle));
        getToolbar().setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.arrow));
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
