package com.app.ersultan.assistantapp.model;

/**
 * Created by ersultan on 10/26/17.
 */

public class FilterVariantsModel {

    public String filterTitle = "";
    public int filterCheckIcon = 0;

    public FilterVariantsModel(){}

    public String getFilterTitle() {
        return filterTitle;
    }

    public void setFilterTitle(String filterTitle) {
        this.filterTitle = filterTitle;
    }

    public int getFilterCheckIcon() {
        return filterCheckIcon;
    }

    public void setFilterCheckIcon(int filterCheckIcon) {
        this.filterCheckIcon = filterCheckIcon;
    }
}
