package com.app.ersultan.assistantapp.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.activity.DetailedNotification;
import com.app.ersultan.assistantapp.helper.Const;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by ersultan on 11/9/17.
 */

public class FireBaseMessagingService extends FirebaseMessagingService{

    public static final String TAG = FirebaseMessagingService.class.getSimpleName();
    private String venueId = "";
    private String title = "";
    private String text = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if(remoteMessage.getData().size() > 0){
            try {
                Map<String, String> params = remoteMessage.getData();
                JSONObject object = new JSONObject(params);
                venueId = String.valueOf(object.getString("notification_id"));
                title = String.valueOf(object.getString("title"));
                text = String.valueOf(object.getString("body"));
            } catch (JSONException e){
                e.printStackTrace();
            }

            sendNotification(title, text, venueId);
        }
    }

    //This method is only generating push notification
    private void sendNotification(String messageTitle, String messageBody, String venueId){
        PendingIntent pendingIntent = null;
        Intent intent = null;

        // start detailed notification activity
        intent = new Intent(this, DetailedNotification.class);
        intent.putExtra(Const.notificationId, venueId);
        intent.putExtra(Const.fromNotificationActivity, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.app_logo))
                .setSmallIcon(R.mipmap.app_logo)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

}
