package com.app.ersultan.assistantapp.model;

import java.io.Serializable;

/**
 * Created by ersultan on 12/4/17.
 */

public class PriceChildModel implements Serializable {

    private String priceChildTitle = "";
    private String priceChildValue = "";
    private String priceChildId = "";
    private String childPosition = "";
    private String checked = "";

    public PriceChildModel(){}

    public String getPriceChildTitle() {
        return priceChildTitle;
    }

    public void setPriceChildTitle(String priceChildTitle) {
        this.priceChildTitle = priceChildTitle;
    }

    public String getPriceChildValue() {
        return priceChildValue;
    }

    public void setPriceChildValue(String priceChildValue) {
        this.priceChildValue = priceChildValue;
    }

    public String getPriceChildId() {
        return priceChildId;
    }

    public void setPriceChildId(String priceChildId) {
        this.priceChildId = priceChildId;
    }

    public String getChildPosition() {
        return childPosition;
    }

    public void setChildPosition(String childPosition) {
        this.childPosition = childPosition;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }
}
