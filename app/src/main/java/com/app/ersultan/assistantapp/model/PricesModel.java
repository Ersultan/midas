package com.app.ersultan.assistantapp.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ersultan on 12/2/17.
 */

public class PricesModel implements Serializable{

    private String id = "";
    private String title = "";
    private ArrayList<SubPricesModel> subPricesModels = new ArrayList<>();

    public PricesModel(){};

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<SubPricesModel> getSubPricesModels() {
        return subPricesModels;
    }

    public void setSubPricesModels(ArrayList<SubPricesModel> subPricesModels) {
        this.subPricesModels = subPricesModels;
    }
}
