package com.app.ersultan.assistantapp.model;

import java.util.ArrayList;

/**
 * Created by ersultan on 9/28/17.
 */

public class SectionDataModel {

    private String headerTitle;
    private String categoryId;
    private ArrayList<SingleItemModel> allItemsInSection;

    public SectionDataModel(){}

    public SectionDataModel(String headerTitle, ArrayList<SingleItemModel> allItemsInSection){
        this.headerTitle = headerTitle;
        this.allItemsInSection = allItemsInSection;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setCategoryId(String id){
        this.categoryId = id;
    }

    public String getCategoryId(){
        return categoryId;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public ArrayList<SingleItemModel> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(ArrayList<SingleItemModel> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }
}
