package com.app.ersultan.assistantapp.activity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.adapter.ExpandableNavigationAdapter;
import com.app.ersultan.assistantapp.adapter.HeaderFooterAdapter;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.helper.Preferences;
import com.app.ersultan.assistantapp.model.NavigationChildModel;
import com.app.ersultan.assistantapp.model.NavigationGroupModel;
import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.acl.Group;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by ersultan on 9/25/17.
 */

public class BaseDrawerActivity extends BaseActivity {

    public static final String TAG = BaseDrawerActivity.class.getSimpleName();

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.vNavigation)
    NavigationView vNavigation;
    @BindView(R.id.recycler_view_navigation_fragment)
    RecyclerView recyclerView;

    @BindView(R.id.image_view_phone)
    ImageView ivPhone;
    @BindView(R.id.image_view_instagram)
    ImageView ivInstagram;

    private List<Pair<NavigationGroupModel, List<NavigationChildModel>>> data = new LinkedList<>();
    private String theLastGroupId = "";

    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewExpandableItemManager expMgr;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentViewWithoutInject(R.layout.activity_drawer);
        ViewGroup viewGroup = findViewById(R.id.flContentRoot);
        LayoutInflater.from(this).inflate(layoutResID, viewGroup, true);
        bindViews();
        setNavigationDrawerMenu();
    }

    private void prepareRecyclerView(){
        mLayoutManager = new LinearLayoutManager(this);

        // Setup expandable feature and RecyclerView
        expMgr = new RecyclerViewExpandableItemManager(null);

        //adapter
        final ExpandableNavigationAdapter myItemAdapter = new ExpandableNavigationAdapter(BaseDrawerActivity.this, expMgr, data, drawerLayout);
        mWrappedAdapter = expMgr.createWrappedAdapter(myItemAdapter);       // wrap for expanding
        mWrappedAdapter = new HeaderFooterAdapter(mWrappedAdapter);

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();

        // Change animations are enabled by default since support-v7-recyclerview v22.
        // Need to disable them when using animation indicator.
        animator.setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mWrappedAdapter);
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(animator);
        // NOTE: need to disable change animations to ripple effect work properly
        //((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

        expMgr.attachRecyclerView(recyclerView);
    }

    private void setNavigationDrawerMenu(){
        SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);
        AndroidNetworking.get(Const.GET_CATEGORIES)
                .addHeaders("accessToken", sharedPreferences.getString(Preferences.userAccessToken, null))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("statusCode").equals("200")){
                                JSONArray result = response.getJSONArray("result");
                                data = new LinkedList<>();

                                for(int i = 0; i < result.length(); i++){
                                    JSONObject categoriesAndCount = result.getJSONObject(i);

                                    final NavigationGroupModel groupModel = new NavigationGroupModel();
                                    groupModel.setGroupTitle(categoriesAndCount.getString("title"));
                                    groupModel.setGroupIcon(getCorrectIcon(categoriesAndCount.getString("id")));
                                    groupModel.setGroupId(categoriesAndCount.getString("id"));
                                    final List<NavigationChildModel> childModelList = new ArrayList<>();

                                    JSONArray subCats = categoriesAndCount.getJSONArray("sub_cats");
                                    for(int j = 0; j < subCats.length(); j++){
                                        JSONObject subCatObject = subCats.getJSONObject(j);
                                        NavigationChildModel childModel = new NavigationChildModel();
                                        childModel.setChildTitle(subCatObject.getString("title"));
                                        childModel.setChildCount(subCatObject.getString("venues_count"));
                                        childModel.setChildId(subCatObject.getString("id"));
                                        childModelList.add(childModel);
                                    }

                                    data.add(new Pair<>(groupModel, childModelList));

                                    // last group id
                                    theLastGroupId = categoriesAndCount.getString("id");
                                }

                                final List<NavigationChildModel> childModelListEmpty = new ArrayList<>();

                                NavigationGroupModel navigationGroupModel = new NavigationGroupModel();
                                navigationGroupModel.setGroupTitle(getString(R.string.notifications));
                                navigationGroupModel.setGroupIcon(R.drawable.percent);
                                navigationGroupModel.setGroupId(String.valueOf(Integer.parseInt(theLastGroupId) + 1));
                                data.add(new Pair<>(navigationGroupModel, childModelListEmpty));
                                prepareRecyclerView();
                            }

                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    @OnClick(R.id.image_view_phone)
    public void ivPhone(){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:+77025859316"));
        startActivity(intent);
    }

    @OnClick(R.id.image_view_instagram)
    public void ivInstagram(){
        String profile = "midas.almaty";

        Uri uri = Uri.parse("http://instagram.com/_u/" + profile);
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/" + profile)));
        }
    }

    private int getCorrectIcon(String id){
        switch (id){
            case "1":
                return R.drawable.icon_place;
            case "2":
                return R.drawable.explore;
            case "3":
                return R.drawable.messages;
            case "4":
                return R.drawable.photos;
            case "5":
                return R.drawable.review;
            default:
                return 0;
        }
    }

    @Override
    protected void setupToolbar() {
        super.setupToolbar();

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        if(getToolbar() != null){
            getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
