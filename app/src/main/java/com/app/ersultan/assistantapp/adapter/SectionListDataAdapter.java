package com.app.ersultan.assistantapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.activity.DetailedItemActivity;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.model.SingleItemModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

/**
 * Created by ersultan on 9/28/17.
 */

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder>{

    private static final String TAG = SectionListDataAdapter.class.getSimpleName();
    private ArrayList<SingleItemModel> itemsList;
    private Context mContext;

    public SectionListDataAdapter(Context mContext, ArrayList<SingleItemModel> itemsList){
        this.mContext = mContext;
        this.itemsList = itemsList;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item, null);
        return new SingleItemRowHolder(view);
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int position) {
        SingleItemModel singleItemModel = itemsList.get(position);
        holder.type.setText(singleItemModel.getType());
        holder.name.setText(singleItemModel.getName());
        holder.rating.setText(singleItemModel.getRating());
        holder.review.setText(singleItemModel.getReviews());

        holder.tvDiscount.setText("-" + singleItemModel.getDiscount() + "%");
        holder.ivDiscount.setBackgroundResource(R.drawable.discount);

        if(!singleItemModel.getDiscount().equals("0")){
            holder.tvDiscount.setVisibility(View.VISIBLE);
            holder.ivDiscount.setVisibility(View.VISIBLE);
        } else {
            holder.tvDiscount.setVisibility(View.GONE);
            holder.ivDiscount.setVisibility(View.GONE);
        }

        Glide.with(mContext)
                .load(singleItemModel.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .centerCrop()
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder{

        protected TextView type, name, rating, review, tvDiscount;
        protected ImageView image, ivDiscount;

        public SingleItemRowHolder(final View itemView) {
            super(itemView);

            this.type = itemView.findViewById(R.id.institutionType);
            this.name = itemView.findViewById(R.id.institutionName);
            this.rating = itemView.findViewById(R.id.ratingCount);
            this.review = itemView.findViewById(R.id.reviewsCount);
            this.image = itemView.findViewById(R.id.ivInstitution);
            this.tvDiscount = itemView.findViewById(R.id.text_discount);
            this.ivDiscount = itemView.findViewById(R.id.image_discount);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(mContext, DetailedItemActivity.class)
                    .putExtra(Const.venueId, itemsList.get(getAdapterPosition()).getId())
                    .putExtra(Const.venueType, itemsList.get(getAdapterPosition()).getType()));
                }
            });
        }
    }


}
