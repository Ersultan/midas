package com.app.ersultan.assistantapp.model;

/**
 * Created by ersultan on 11/14/17.
 */

public class SortingModel {

    private String id = "";
    private String value = "";
    private String checked = "";

    public SortingModel(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }
}
