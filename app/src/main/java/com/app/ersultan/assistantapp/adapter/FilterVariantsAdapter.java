package com.app.ersultan.assistantapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.model.FilterVariantsModel;

import java.util.List;

/**
 * Created by ersultan on 10/26/17.
 */

public class FilterVariantsAdapter extends RecyclerView.Adapter<FilterVariantsAdapter.MyViewHolder> {

    private Context context;
    private List<FilterVariantsModel> filterVariantsModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvFilterTitle;
        public ImageView ivFilterCheckIcon;

        public MyViewHolder(View itemView){
            super(itemView);
            tvFilterTitle = itemView.findViewById(R.id.text_filter_variants_title);
            ivFilterCheckIcon = itemView.findViewById(R.id.image_check);
        }
    }

    public FilterVariantsAdapter(Context context, List<FilterVariantsModel> filterVariantsModels){
        this.context = context;
        this.filterVariantsModelList = filterVariantsModels;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_filter_variants, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FilterVariantsModel filterVariantsModel = filterVariantsModelList.get(position);
        holder.tvFilterTitle.setText(filterVariantsModel.getFilterTitle());
        holder.ivFilterCheckIcon.setBackgroundResource(filterVariantsModel.getFilterCheckIcon());
    }

    @Override
    public int getItemCount() {
        return filterVariantsModelList.size();
    }
}
