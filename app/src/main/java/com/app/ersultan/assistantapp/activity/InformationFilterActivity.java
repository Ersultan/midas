package com.app.ersultan.assistantapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.model.InformationModel;

import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.OnClick;

public class InformationFilterActivity extends BaseActivity {

    private static final String TAG = InformationFilterActivity.class.getSimpleName();
    @BindView(R.id.text_information_description)
    TextView tvDescription;
    @BindView(R.id.text_sum)
    TextView tvSum;
    @BindView(R.id.text_min_value)
    TextView tvMinValue;
    @BindView(R.id.text_max_value)
    TextView tvMaxValue;
    @BindView(R.id.seek_bar_sum)
    SeekBar sbSum;

    @BindView(R.id.image_view_minus)
    ImageView ivMinus;
    @BindView(R.id.image_view_plus)
    ImageView ivPlus;

    private InformationModel informationModel = new InformationModel();
    private int informationCurrentListPosition = 0;
    private int layoutPosition = 0;
    private String endString = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_filter);
        setToolbar();
        setInformationValues();
    }

    private void setInformationValues(){
        Intent intent = getIntent();
        informationModel = (InformationModel) intent.getSerializableExtra(Const.filterModel);

        if(!informationModel.getDescription().equals("")){
            tvDescription.setVisibility(View.VISIBLE);
            tvDescription.setText(informationModel.getDescription());
        } else {
            tvDescription.setVisibility(View.GONE);
        }

        informationCurrentListPosition = intent.getIntExtra(Const.informationPosition, 0);
        layoutPosition = intent.getIntExtra(Const.layoutPosition, 0);
        if(!informationModel.getEndString().equals("")){
            endString = " " + informationModel.getEndString();
        }

        tvMinValue.setText(informationModel.getMinValue() + endString);
        tvMaxValue.setText(informationModel.getMaxValue() + endString);
        sbSum.setMax(Integer.parseInt(informationModel.getMaxValue()) - Integer.parseInt(informationModel.getMinValue()));

        if(!informationModel.getValue().equals("")){
            informationModel.setValue(informationModel.getValue().replaceAll(endString, ""));
            tvSum.setText(informationModel.getValue() + endString);
            sbSum.setProgress(Integer.parseInt(informationModel.getValue()) - Integer.parseInt(informationModel.getMinValue()));
        } else {
            tvSum.setText(informationModel.getMinValue() + endString);
            //sbSum.setProgress(Integer.parseInt(informationModel.getMinValue()));
        }

        sbSum.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChanged = Integer.parseInt(informationModel.getMinValue());

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progressChanged = Integer.parseInt(informationModel.getMinValue()) + i;
                tvSum.setText(String.valueOf(progressChanged) + endString);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @OnClick(R.id.image_view_minus)
    public void minus(){
        int currentValue = sbSum.getProgress();
        int conditionValue = Integer.parseInt(informationModel.getMinValue()) + currentValue;
        Log.i(TAG, "MINUS CURRENT VALUE: " + currentValue);
        Log.i(TAG, "Conditoin value: " + conditionValue);
        if(conditionValue > Integer.parseInt(informationModel.getMinValue())){
            int progressChanged = (Integer.parseInt(informationModel.getMinValue()) + sbSum.getProgress()) - 1;
            sbSum.setProgress(sbSum.getProgress() - 1);
            tvSum.setText(String.valueOf(progressChanged) + endString);
        }
    }

    @OnClick(R.id.image_view_plus)
    public void plus(){
        Log.i(TAG, "PLUS CLICKED");
        Log.i(TAG, "PROGRESS VALUE: " + sbSum.getProgress());
        int currentValue = sbSum.getProgress();
        int conditionValue = Integer.parseInt(informationModel.getMinValue()) + currentValue;
        if(conditionValue < Integer.parseInt(informationModel.getMaxValue())){
            int progressChanged = Integer.parseInt(informationModel.getMinValue()) + 1;
            tvSum.setText(String.valueOf(progressChanged) + endString);
            sbSum.setProgress(sbSum.getProgress() + 1);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getSearchMenuItem().setVisible(false);
        getFilterMenuItem().setVisible(false);
        getOkMenuItem().setVisible(true);
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_ok:
                informationModel.setValue(tvSum.getText().toString());

                Intent intent = new Intent();
                intent.putExtra(Const.filterModel, informationModel);
                intent.putExtra(Const.informationPosition, informationCurrentListPosition);
                intent.putExtra(Const.layoutPosition, layoutPosition);
                setResult(RESULT_OK, intent);
                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setToolbar(){
        Intent intent = getIntent();
        getTvToolbarTitle().setText(intent.getStringExtra(Const.filterTitle));
        getToolbar().setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.arrow));
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
