package com.app.ersultan.assistantapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.ersultan.assistantapp.CheckUserStatus;
import com.app.ersultan.assistantapp.MainActivity;
import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.adapter.NotificationAdapter;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.helper.Preferences;
import com.app.ersultan.assistantapp.helper.RecyclerTouchListener;
import com.app.ersultan.assistantapp.model.NotificationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class NotificationsActivity extends BaseDrawerActivity implements SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = NotificationsActivity.class.getSimpleName();
    @BindView(R.id.recycler_view_notifications)
    RecyclerView recyclerView;
    @BindView(R.id.progress_bar_notifications)
    ProgressBar pbLoadMoreNotifications;
    @BindView(R.id.relative_layout_notifications_content)
    RelativeLayout rlContent;
    private List<NotificationModel> notificationModelList = new ArrayList<>();
    private NotificationAdapter adapter;
    private SharedPreferences sharedPreferences;

    @BindView(R.id.progress_bar_under_notifications_recycler_view)
    ProgressBar pbUnderNotifications;
    @BindView(R.id.nested_scroll_view_notifications)
    NestedScrollView scrollView;
    @BindView(R.id.swipe_refresh_layout_notifications)
    SwipeRefreshLayout swipeRefreshLayout;

    private int currentPage = 1;
    private boolean allDataLoaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        setToolbar();

        sharedPreferences = getSharedPreferences(Preferences.shPref, 0);

        swipeRefreshLayout.setOnRefreshListener(this);

        getNotifications(String.valueOf(currentPage), false);
        prepareRecyclerView();
    }

    private void prepareRecyclerView(){
        recyclerView.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(NotificationsActivity.this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new NotificationAdapter(NotificationsActivity.this, notificationModelList);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(NotificationsActivity.this, DetailedNotification.class);
                intent.putExtra(Const.notificationId, notificationModelList.get(position).getId());
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) { }
        }));

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if(!allDataLoaded){
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                currentPage = currentPage + 1;
                                getNotifications(String.valueOf(currentPage), false);
                            }
                        }, 50);
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(NotificationsActivity.this, MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
        finish();
    }

    private void getNotifications(final String pageNumber, final boolean fromSwipe){
        if(fromSwipe){
            swipeRefreshLayout.setRefreshing(true);
        }

        Log.i(TAG, "GET NOTIFICATIONS CURRENT PAGE: " + pageNumber);
        AndroidNetworking.get(Const.URL_GET_ALL_NOTIFICATIONS)
                .addHeaders(Const.accessToken, sharedPreferences.getString(Preferences.userAccessToken, null))
                .addQueryParameter("page", pageNumber)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("statusCode").equals("200")){
                                JSONObject result = response.getJSONObject("result");
                                JSONArray notifications = result.getJSONArray("notifications");
                                for(int i = 0; i < notifications.length(); i++){
                                    JSONObject notificationObject = notifications.getJSONObject(i);
                                    NotificationModel notificationModel = new NotificationModel();
                                    notificationModel.setId(notificationObject.getString("id"));
                                    notificationModel.setDate(notificationObject.getString("date"));
                                    notificationModel.setDescription(notificationObject.getString("description"));
                                    notificationModel.setText(notificationObject.getString("text"));
                                    notificationModel.setTitle(notificationObject.getString("title"));
                                    notificationModelList.add(notificationModel);
                                }
                                adapter.notifyDataSetChanged();
                                pbLoadMoreNotifications.setVisibility(View.GONE);
                                rlContent.setVisibility(View.VISIBLE);

                                if(fromSwipe){
                                    swipeRefreshLayout.setRefreshing(false);
                                    pbUnderNotifications.setVisibility(View.VISIBLE);
                                }

                                if(notifications.length() == 0){
                                    pbUnderNotifications.setVisibility(View.GONE);
                                    allDataLoaded = true;
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject response = new JSONObject(anError.getErrorBody());
                            if(response.getString("statusCode").equals("401")){

                                Toast.makeText(NotificationsActivity.this, getString(R.string.session_error), Toast.LENGTH_LONG).show();

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Preferences.userAccessToken, null);
                                editor.putBoolean(Preferences.logged, false);
                                editor.commit();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(NotificationsActivity.this, CheckUserStatus.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    }
                                }, 3500);
                            } else if(response.getString("statusCode").equals("500")){
                                Toast.makeText(NotificationsActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                                rlContent.setVisibility(View.GONE);
                                pbUnderNotifications.setVisibility(View.GONE);
                                pbLoadMoreNotifications.setVisibility(View.GONE);

                            } else if(response.getString("statusCode").equals("408")){
                                Toast.makeText(NotificationsActivity.this, getString(R.string.timeout_error), Toast.LENGTH_LONG).show();
                                rlContent.setVisibility(View.GONE);
                                pbUnderNotifications.setVisibility(View.GONE);
                                pbLoadMoreNotifications.setVisibility(View.GONE);
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void setToolbar(){
        getTvToolbarTitle().setText(R.string.notifications);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getSearchMenuItem().setVisible(false);
        getFilterMenuItem().setVisible(false);
        getOkMenuItem().setVisible(false);
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public void onRefresh() {
        currentPage = 1;
        notificationModelList.clear();
        allDataLoaded = false;
        getNotifications(String.valueOf(currentPage), true);
    }
}
