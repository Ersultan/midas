package com.app.ersultan.assistantapp.model;

import java.io.Serializable;

/**
 * Created by ersultan on 10/23/17.
 */

public class DistrictModel implements Serializable{

    public String districtTitle = "";
    public String districtId = "";
    public String checked = "";

    public DistrictModel(){}

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public String getDistrictTitle() {
        return districtTitle;
    }

    public void setDistrictTitle(String districtTitle) {
        this.districtTitle = districtTitle;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }
}
