package com.app.ersultan.assistantapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.ersultan.assistantapp.activity.BaseDrawerActivity;
import com.app.ersultan.assistantapp.activity.DetailedItemActivity;
import com.app.ersultan.assistantapp.activity.VenueWithFiltersActivity;
import com.app.ersultan.assistantapp.adapter.MoreVenueAdapter;
import com.app.ersultan.assistantapp.adapter.RecyclerViewDataAdapter;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.helper.GridSpacingItemDecoration;
import com.app.ersultan.assistantapp.helper.Preferences;
import com.app.ersultan.assistantapp.helper.RecyclerTouchListener;
import com.app.ersultan.assistantapp.model.SectionDataModel;
import com.app.ersultan.assistantapp.model.SingleItemModel;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import dmax.dialog.SpotsDialog;

public class MainActivity extends BaseDrawerActivity implements SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = MainActivity.class.getSimpleName();
    private ArrayList<SectionDataModel> allDataList = new ArrayList<>();
    @BindView(R.id.main_recycler_view)
    RecyclerView main_recycler_view;
    @BindView(R.id.mainProgressView)
    ProgressBar progressBar;
    @BindView(R.id.mainContent)
    LinearLayout mainContent;
    @BindView(R.id.slider_layout_banner)
    SliderLayout sliderLayout;

    @BindView(R.id.nested_scroll_view_main)
    NestedScrollView nestedScrollView;
    @BindView(R.id.swipe_refresh_layout_main_content)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.material_search_view_main)
    MaterialSearchView searchView;
    private AlertDialog customProgressDialog = null;

    @BindView(R.id.text_request_result_main)
    TextView tvNotFound;

    private MoreVenueAdapter adapterSearch;
    private List<SingleItemModel> venuesListSearch = new ArrayList<>();
    @BindView(R.id.recycler_view_main_search)
    RecyclerView recyclerViewSearch;
    @BindView(R.id.progress_bar_main_loading_search_venues)
    ProgressBar pbLoadingVenues;
    private boolean allSearchDataLoaded = false;
    private int currentSearchPage = 1;
    private String typedSearch = "";

    private ArrayList<String> sliderImagesUrl = new ArrayList<>();
    private ArrayList<String> bannerLinks = new ArrayList<>();

    private RecyclerViewDataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToolbar();

        swipeRefreshLayout.setOnRefreshListener(this);
        customProgressDialog = new SpotsDialog(this, R.style.CustomProgressDialog);
        prepareSearchRecyclerView();
        searchViewListener();

        mainContent.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadVenues();
                prepareRecyclerView();
            }
        }, 1000);
        super.onResume();
    }

    @Override
    protected void onStop() {
        sliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    protected void onRestart() {
        sliderLayout.startAutoCycle(5000, 5000, false);
        super.onRestart();
    }

    private void prepareRecyclerView(){
        adapter = new RecyclerViewDataAdapter(MainActivity.this, allDataList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        main_recycler_view.setLayoutManager(layoutManager);
        main_recycler_view.setHasFixedSize(true);
        main_recycler_view.setNestedScrollingEnabled(false);
        main_recycler_view.setAdapter(adapter);
    }

    private void loadVenues(){
        final SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);

        AndroidNetworking.get(Const.MAIN_URL)
            .addHeaders("accessToken", sharedPreferences.getString(Preferences.userAccessToken, null))
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if(response.getString("statusCode").equals("200")){
                            JSONObject result = response.getJSONObject("result");
                            JSONArray main = result.getJSONArray("main");

                            // set slider
                            if(sliderImagesUrl.size() == 0){
                                JSONArray banner = result.getJSONArray("banner");
                                for(int i = 0; i < banner.length(); i++){
                                    JSONObject bannerObject = banner.getJSONObject(i);
                                    sliderImagesUrl.add(Const.DOMAIN_URL + bannerObject.getString("image"));
                                    bannerLinks.add(bannerObject.getString("link"));
                                }

                                setImages();
                            }

                            // fill items
                            allDataList.clear();
                            for(int i = 0; i < main.length(); i++){
                                JSONObject resultItem = main.getJSONObject(i);
                                SectionDataModel sdm = new SectionDataModel();
                                sdm.setHeaderTitle(resultItem.getString("title"));
                                sdm.setCategoryId(resultItem.getString("id"));

                                ArrayList<SingleItemModel> singleItem = new ArrayList<>();
                                JSONArray venues = resultItem.getJSONArray("venues");
                                if(venues.length() > 0) {
                                    for (int j = 0; j < venues.length(); j++) {
                                        JSONObject venuesItem = venues.getJSONObject(j);
                                        SingleItemModel singleItemModel = new SingleItemModel();
                                        singleItemModel.setId(venuesItem.getString("id"));
                                        singleItemModel.setImageURL(Const.DOMAIN_URL + venuesItem.getString("image"));
                                        singleItemModel.setReviews(venuesItem.getString("reviews_count"));
                                        singleItemModel.setRating(venuesItem.getString("rating"));
                                        singleItemModel.setType(venuesItem.getString("category_title"));
                                        singleItemModel.setName(venuesItem.getString("title"));
                                        singleItemModel.setDiscount(venuesItem.getString("discount"));
                                        singleItem.add(singleItemModel);
                                    }

                                    sdm.setAllItemsInSection(singleItem);
                                    allDataList.add(sdm);
                                }
                            }
                        }

                        if(recyclerViewSearch.getVisibility() == View.GONE){
                            mainContent.setVisibility(View.VISIBLE);
                        }

                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    try {
                        JSONObject response = new JSONObject(anError.getErrorBody());
                        if(response.getString("statusCode").equals("401")){

                            Toast.makeText(MainActivity.this, getString(R.string.session_error), Toast.LENGTH_LONG).show();

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Preferences.userAccessToken, null);
                            editor.putBoolean(Preferences.logged, false);
                            editor.commit();

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    startActivity(new Intent(MainActivity.this, CheckUserStatus.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                }
                            }, 3500);
                        } else if(response.getString("statusCode").equals("500")){
                            Toast.makeText(MainActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            mainContent.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);

                        } else if(response.getString("statusCode").equals("408")){
                            Toast.makeText(MainActivity.this, getString(R.string.timeout_error), Toast.LENGTH_LONG).show();
                            mainContent.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }

//                    Log.i(TAG, anError.getErrorBody().toString());
//                    Log.i(TAG, anError.getErrorDetail().toString());
//                    Log.i(TAG, anError.getResponse().toString());
//                    Log.i(TAG, "ERROR CODE: "  + anError.getErrorCode());
                }
            });
    }

    private void setImages(){
        sliderLayout.removeAllSliders();
        sliderLayout.stopAutoCycle();
        //sliderLayout.removeAllSliders();
        sliderLayout.setIndicatorVisibility(null);
        for(int i = 0; i < sliderImagesUrl.size(); i++){
            DefaultSliderView defaultSliderView = new DefaultSliderView(MainActivity.this);
            defaultSliderView.image(sliderImagesUrl.get(i)).setScaleType(BaseSliderView.ScaleType.CenterCrop);
            sliderLayout.addSlider(defaultSliderView);
        }

        sliderLayout.setCurrentPosition(0);
        sliderLayout.startAutoCycle(5000, 5000, false);
        sliderLayout.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

            @Override
            public void onPageSelected(int position) {
                sliderClickListener();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                sliderLayout.startAutoCycle(5000, 5000, false);
            }
        });

        sliderClickListener();
    }

    private void sliderClickListener(){
        sliderLayout.getCurrentSlider().setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
            @Override
            public void onSliderClick(BaseSliderView slider) {
                String url = bannerLinks.get(sliderLayout.getCurrentPosition());
                Intent browserIntent = null;
                if(url.startsWith("http://") || url.startsWith("https://"))
                    browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                else
                    browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + url));
                startActivity(browserIntent);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        sliderLayout.stopAutoCycle();
    }

    public void setToolbar(){
        getTvToolbarTitle().setText(R.string.main);
    }

    public void searchViewListener(){
        searchView.setHint(getString(R.string.search_hint_text));
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                progressBar.setVisibility(View.GONE);
                mainContent.setVisibility(View.GONE);
                customProgressDialog.show();
                recyclerViewSearch.setVisibility(View.VISIBLE);
                loadSearchedVenues(typedSearch);
            }

            @Override
            public void onSearchViewClosed() {
                setDefaultValues();
                pbLoadingVenues.setVisibility(View.GONE);
                recyclerViewSearch.setVisibility(View.GONE);
                mainContent.setVisibility(View.VISIBLE);
            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                setDefaultValues();
                customProgressDialog.show();
                typedSearch = query;
                loadSearchedVenues(typedSearch);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
    }

    private void loadSearchedVenues(String venueName){
        final SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);

        AndroidNetworking.get(Const.URL_MAIN_PAGE_SEARCH + venueName)
                .addHeaders("accessToken", sharedPreferences.getString(Preferences.userAccessToken, null))
                .addQueryParameter("page", String.valueOf(currentSearchPage))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("statusCode").equals("200")){
                                JSONObject result = response.getJSONObject("result");
                                JSONArray venues = result.getJSONArray("venues");
                                for(int i = 0; i < venues.length(); i++){
                                    JSONObject venuesItem = venues.getJSONObject(i);
                                    SingleItemModel singleItemModel = new SingleItemModel();
                                    singleItemModel.setId(venuesItem.getString("id"));
                                    singleItemModel.setImageURL(Const.DOMAIN_URL + venuesItem.getString("image"));
                                    singleItemModel.setRating(venuesItem.getString("rating"));
                                    singleItemModel.setType(venuesItem.getString("category_title"));
                                    singleItemModel.setName(venuesItem.getString("title"));
                                    singleItemModel.setReviews(venuesItem.getString("reviews_count"));
                                    singleItemModel.setDiscount(venuesItem.getString("discount"));
                                    venuesListSearch.add(singleItemModel);
                                }

                                JSONObject details = result.getJSONObject("details");
                                int totalCount = details.getInt("count");
                                if(venues.length() == 0 || venuesListSearch.size() == totalCount){
                                    allSearchDataLoaded = true;
                                    pbLoadingVenues.setVisibility(View.GONE);
                                } else {
                                    allSearchDataLoaded = false;
                                    pbLoadingVenues.setVisibility(View.VISIBLE);
                                }

                                if(venuesListSearch.size() == 0){
                                    tvNotFound.setVisibility(View.VISIBLE);
                                } else {
                                    tvNotFound.setVisibility(View.GONE);
                                }

                                swipeRefreshLayout.setRefreshing(false);
                                customProgressDialog.dismiss();
                                adapterSearch.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject response = new JSONObject(anError.getErrorBody());
                            if (response.getString("statusCode").equals("401")) {

                                Toast.makeText(MainActivity.this, getString(R.string.session_error), Toast.LENGTH_LONG).show();

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Preferences.userAccessToken, null);
                                editor.putBoolean(Preferences.logged, false);
                                editor.commit();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(MainActivity.this, CheckUserStatus.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    }
                                }, 3500);
                            } else if (response.getString("statusCode").equals("500")) {
                                Toast.makeText(MainActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                                mainContent.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);
                                customProgressDialog.dismiss();
                                swipeRefreshLayout.setRefreshing(false);

                            } else if (response.getString("statusCode").equals("408")) {
                                Toast.makeText(MainActivity.this, getString(R.string.timeout_error), Toast.LENGTH_LONG).show();
                                mainContent.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);
                                customProgressDialog.dismiss();
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void prepareSearchRecyclerView(){
        adapterSearch = new MoreVenueAdapter(MainActivity.this, venuesListSearch);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerViewSearch.setLayoutManager(mLayoutManager);
        recyclerViewSearch.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(5), true));
        recyclerViewSearch.setItemAnimator(new DefaultItemAnimator());
        recyclerViewSearch.setNestedScrollingEnabled(false);
        recyclerViewSearch.setAdapter(adapterSearch);

        recyclerViewSearch.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerViewSearch, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(MainActivity.this, DetailedItemActivity.class);
                intent.putExtra(Const.venueId, venuesListSearch.get(position).getId());
                intent.putExtra(Const.venueType, venuesListSearch.get(position).getType());
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) { }
        }));

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if(!allSearchDataLoaded){
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(searchView.isSearchOpen()){
                                    if(!allSearchDataLoaded){
                                        currentSearchPage = currentSearchPage + 1;
                                        loadSearchedVenues(typedSearch);
                                    }
                                }
                            }
                        }, 50);
                    }
                }
            }
        });
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        searchView.setMenuItem(getSearchMenuItem());
        getOkMenuItem().setVisible(false);
        getFilterMenuItem().setVisible(false);
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            finish();
        }
    }

    @Override
    public void onRefresh() {
        if(searchView.isSearchOpen()){
            setDefaultValues();
            loadSearchedVenues(typedSearch);
        } else {
            loadVenues();
        }
    }

    private void setDefaultValues(){
        currentSearchPage = 1;
        typedSearch = "";
        venuesListSearch.clear();
        adapterSearch.notifyDataSetChanged();
    }

}
