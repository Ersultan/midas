package com.app.ersultan.assistantapp.model;

import java.io.Serializable;

/**
 * Created by ersultan on 11/2/17.
 */

public class NotificationModel implements Serializable{

    private String id = "";
    private String title = "";
    private String description = "";
    private String text = "";
    private String date = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
