package com.app.ersultan.assistantapp.model;

import java.io.Serializable;

/**
 * Created by ersultan on 10/17/17.
 */

public class FullScreenImageModel implements Serializable {
    private String image;

    public FullScreenImageModel(){}

    public FullScreenImageModel(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
