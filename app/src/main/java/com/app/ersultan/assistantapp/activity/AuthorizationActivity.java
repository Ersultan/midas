package com.app.ersultan.assistantapp.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.ersultan.assistantapp.MainActivity;
import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.helper.Preferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.transitionseverywhere.TransitionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmax.dialog.SpotsDialog;

public class AuthorizationActivity extends AppCompatActivity {

    public static final String TAG = AuthorizationActivity.class.getSimpleName();

    // send code activity
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etPhone)
    EditText etPhone;
    @BindView(R.id.btnSendCode)
    Button btnSendCode;

    @BindView(R.id.etConfirmCode)
    EditText etConfirmCode;
    @BindView(R.id.btnSignIn)
    Button btnSignIn;

    private String mVerificationId = "";
    private PhoneAuthProvider.ForceResendingToken mResendToken = null;
    private String currentUserPhone = "";
    private ViewGroup transitionContainer = null;
    private String currentStatusCode = "";
    private String jwt = "";

    private AlertDialog customProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidNetworking.initialize(getApplicationContext());
        setContentView(R.layout.activity_authorization);
        ButterKnife.bind(this);

        customProgressDialog = new SpotsDialog(this, R.style.CustomProgressDialog);
        customProgressDialog.setCancelable(false);
        transitionContainer = findViewById(R.id.transition_container);

        final MaskedTextChangedListener listener = new MaskedTextChangedListener(
                "+7 ([000]) [000] [00] [00]",
                true,
                etPhone,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {
                        if(String.valueOf(maskFilled).equals("true")){
                            currentUserPhone = "+7" + extractedValue;
                        }
                    }
                }
        );

        etPhone.addTextChangedListener(listener);
        etPhone.setOnFocusChangeListener(listener);
    }

    @OnClick(R.id.btnSendCode)
    public void btnSendCode(){
        if(!TextUtils.isEmpty(etName.getText().toString())){

            if(etPhone.getText().toString().length() == 18){
                Resources r = getResources();
                int left = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, r.getDisplayMetrics());
                int bottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics());

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) etPhone.getLayoutParams();
                params.setMargins(left, 0, left, bottom);
                etPhone.setLayoutParams(params);

                etName.setFocusable(false);
                etPhone.setFocusable(false);

                TransitionManager.beginDelayedTransition(transitionContainer);
                etConfirmCode.setVisibility(View.VISIBLE);
                btnSendCode.setVisibility(View.GONE);
                btnSignIn.setVisibility(View.VISIBLE);

                getCode();

            } else Toast.makeText(AuthorizationActivity.this, getString(R.string.fill_phone), Toast.LENGTH_SHORT).show();

        } else Toast.makeText(AuthorizationActivity.this, getString(R.string.fill_name), Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btnSignIn)
    public void btnSignIn(){
        if(!TextUtils.isEmpty(etConfirmCode.getText().toString())){
            if(mVerificationId != ""){
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, etConfirmCode.getText().toString());
                signInWithPhoneAuthCredential(credential);
            } else {
                Toast.makeText(AuthorizationActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
                onBackPressed();
            }
        } else {
            Toast.makeText(AuthorizationActivity.this, getString(R.string.fill_confirm_code), Toast.LENGTH_SHORT).show();
        }
    }

    private void signInWithPhoneAuthCredential(final PhoneAuthCredential credential) {
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.i(TAG, "Task is successfully");
                            FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
                            mUser.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                                @Override
                                public void onComplete(@NonNull Task<GetTokenResult> taskJWT) {
                                    if(taskJWT.isSuccessful()){
                                        jwt = taskJWT.getResult().getToken();
                                        sendLoginRequest();
                                        customProgressDialog.show();
                                    }
                                }
                            });
                        } else {
                            if(task.getException() instanceof FirebaseAuthInvalidCredentialsException){
                                Toast.makeText(AuthorizationActivity.this, getString(R.string.incorrect_sms_code), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(AuthorizationActivity.this, getString(R.string.incorrect_sms_code), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }

    private void sendLoginRequest(){
        Map<String, String> params = new HashMap<>();
        params.put("name", etName.getText().toString());
        params.put("jwt", jwt);

        AndroidNetworking.post(Const.URL_LOGIN)
                .addBodyParameter(params)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            currentStatusCode = response.getString("statusCode");
                            SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Preferences.userAccessToken, response.getJSONObject("result").getString("accessToken"));
                            editor.commit();

                            if (currentStatusCode.equals("201")) {
                                Toast.makeText(AuthorizationActivity.this, getString(R.string.successfully), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(AuthorizationActivity.this, AcceptanceActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK)
                                        .putExtra(Const.agreementPage, response.getJSONObject("result").getString("agreement")));

                                customProgressDialog.dismiss();
                            } else if (currentStatusCode.equals("202")) {
                                addFCMToken();
                            }

                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.i(TAG, anError.getErrorBody().toString());
                        Log.i(TAG, anError.getErrorDetail().toString());
                        Log.i(TAG, String.valueOf(anError.getErrorCode()));
                        Log.i(TAG, anError.getResponse().toString());
                    }
                });
    }

    private void addFCMToken(){
        Map<String, String> params = new HashMap<>();
        params.put("fcm_token", FirebaseInstanceId.getInstance().getToken());
        params.put("device_type", "2");

        SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);

        AndroidNetworking.post(Const.URL_ADD_FCM_TOKEN)
                .addBodyParameter(params)
                .addHeaders("accessToken", sharedPreferences.getString(Preferences.userAccessToken, null))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i(TAG, "Status Code: " + response.getString("statusCode"));
                            Log.i(TAG, "Current Status code: " + currentStatusCode);
                            int stCode = Integer.parseInt(response.getString("statusCode").toString());

                            if(stCode >= 200 && stCode < 300){
                                SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean(Preferences.logged, true);
                                editor.commit();

                                Toast.makeText(AuthorizationActivity.this, getString(R.string.successfully), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(AuthorizationActivity.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

                                customProgressDialog.dismiss();
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                    }
                });
    }

    private void getCode(){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                currentUserPhone,         // Phone number to verify
                60,                    // Timeout duration
                TimeUnit.SECONDS,        // Unit of timeout
                this,            // Activity (for callback binding)
                mCallbacks);
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            signInWithPhoneAuthCredential(phoneAuthCredential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                Toast.makeText(AuthorizationActivity.this, getString(R.string.incorrect_request), Toast.LENGTH_SHORT).show();
                onBackPressed();
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                Toast.makeText(AuthorizationActivity.this, getString(R.string.time_finished), Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        }

        @Override
        public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
            // Save verification ID and resending token so we can use them later
            mVerificationId = verificationId;
            mResendToken = token;
        }
    };

    @Override
    public void onBackPressed() {
        if(etConfirmCode.getVisibility() == View.VISIBLE){
            TransitionManager.beginDelayedTransition(transitionContainer);
            etConfirmCode.setText("");
            etConfirmCode.setVisibility(View.GONE);
            btnSendCode.setVisibility(View.VISIBLE);
            btnSignIn.setVisibility(View.GONE);
            etPhone.setFocusableInTouchMode(true);
            etName.setFocusableInTouchMode(true);
            etName.setFocusable(true);
            etPhone.setFocusable(true);
        } else {
            finish();
        }
    }
}
