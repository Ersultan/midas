package com.app.ersultan.assistantapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.app.ersultan.assistantapp.MainActivity;
import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.helper.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartWorkActivity extends AppCompatActivity {

    public static final String TAG = StartWorkActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_work);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);
                boolean logged = sharedPreferences.getBoolean(Preferences.logged, false);

                if(logged){
                    startActivity(new Intent(StartWorkActivity.this, MainActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                } else {
                    startActivity(new Intent(StartWorkActivity.this, AuthorizationActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                }
            }
        }, 1500);
    }
}
