package com.app.ersultan.assistantapp.model;

/**
 * Created by ersultan on 10/19/17.
 */

public class ReviewModel {

    private String userName = "";
    private String userReview = "";
    private String reviewTime = "";

    public ReviewModel(){}

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserReview() {
        return userReview;
    }

    public void setUserReview(String userReview) {
        this.userReview = userReview;
    }

    public String getReviewTime() {
        return reviewTime;
    }

    public void setReviewTime(String reviewTime) {
        this.reviewTime = reviewTime;
    }
}
