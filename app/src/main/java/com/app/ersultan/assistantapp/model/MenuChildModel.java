package com.app.ersultan.assistantapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ersultan on 11/9/17.
 */

public class MenuChildModel {

    private String menuChildTitle = "";
    private String menuChildPrice = "";
    private String menuChildId = "";
    private String childPosition = "";
    private boolean checked = false;

    public MenuChildModel(){}

    protected MenuChildModel(Parcel in) {
        menuChildTitle = in.readString();
        menuChildPrice = in.readString();
        menuChildId = in.readString();
        childPosition = in.readString();
        checked = in.readByte() != 0;
    }

    public String getChildPosition() {
        return childPosition;
    }

    public void setChildPosition(String childPosition) {
        this.childPosition = childPosition;
    }

    public String getMenuChildTitle() {
        return menuChildTitle;
    }

    public void setMenuChildTitle(String menuChildTitle) {
        this.menuChildTitle = menuChildTitle;
    }

    public String getMenuChildPrice() {
        return menuChildPrice;
    }

    public void setMenuChildPrice(String menuChildPrice) {
        this.menuChildPrice = menuChildPrice;
    }

    public String getMenuChildId() {
        return menuChildId;
    }

    public void setMenuChildId(String menuChildId) {
        this.menuChildId = menuChildId;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
