package com.app.ersultan.assistantapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.adapter.ExpandablePriceAdapter;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.model.PriceChildModel;
import com.app.ersultan.assistantapp.model.PriceGroupModel;
import com.app.ersultan.assistantapp.model.PricesModel;
import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class PriceFilterActivity extends BaseActivity {

    public static final String TAG = PriceFilterActivity.class.getSimpleName();

    private ArrayList<PricesModel> pricesModelArrayList = new ArrayList<>();

    @BindView(R.id.recycler_view_price)
    RecyclerView recyclerView;

    private List<Pair<PriceGroupModel, List<PriceChildModel>>> data = new LinkedList<>();
    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewExpandableItemManager expMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_filter);

        Intent intent = getIntent();
        pricesModelArrayList = (ArrayList<PricesModel>) intent.getSerializableExtra(Const.filterArray);

        setToolbar();
        setPrices();
    }

    private void prepareRecyclerView(){
        mLayoutManager = new LinearLayoutManager(this);

        // Setup expandable feature and RecyclerView
        expMgr = new RecyclerViewExpandableItemManager(null);

        //adapter
        final ExpandablePriceAdapter myItemAdapter = new ExpandablePriceAdapter(PriceFilterActivity.this, expMgr, data);
        //final ExpandablePriceAdapter myItemAdapter = new ExpandablePriceAdapter(PriceFilterActivity.this, expMgr, data);
        mWrappedAdapter = expMgr.createWrappedAdapter(myItemAdapter);       // wrap for expanding

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();

        // Change animations are enabled by default since support-v7-recyclerview v22.
        // Need to disable them when using animation indicator.
        animator.setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mWrappedAdapter);
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setItemAnimator(animator);
        // NOTE: need to disable change animations to ripple effect work properly
        //((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

        expMgr.attachRecyclerView(recyclerView);
    }

    private void setPrices(){
        for(int i = 0; i < pricesModelArrayList.size(); i++){

            final PriceGroupModel groupModel = new PriceGroupModel();
            groupModel.setPriceGroupId(pricesModelArrayList.get(i).getId());
            groupModel.setPriceGroupTitle(pricesModelArrayList.get(i).getTitle());

            final List<PriceChildModel> childModelsList = new ArrayList<>();
            for(int j = 0; j < pricesModelArrayList.get(i).getSubPricesModels().size(); j++){
                PriceChildModel childModel = new PriceChildModel();
                childModel.setPriceChildId(pricesModelArrayList.get(i).getSubPricesModels().get(j).getId());
                childModel.setPriceChildTitle(pricesModelArrayList.get(i).getSubPricesModels().get(j).getTitle());
                childModel.setChecked(pricesModelArrayList.get(i).getSubPricesModels().get(j).getChecked());
                if(!pricesModelArrayList.get(i).getSubPricesModels().get(j).getValue().equals("")){
                    childModel.setPriceChildValue(pricesModelArrayList.get(i).getSubPricesModels().get(j).getValue());
                } else {
                    childModel.setPriceChildValue(getString(R.string.price_with_symbol));
                }

                if(j != pricesModelArrayList.get(i).getSubPricesModels().size() - 1)
                    childModel.setChildPosition("first or middle");
                else if(j == pricesModelArrayList.get(i).getSubPricesModels().size() - 1)
                    childModel.setChildPosition("last");

                childModelsList.add(childModel);
            }

            data.add(new Pair<>(groupModel, childModelsList));
        }

        prepareRecyclerView();
    }

    private void setToolbar(){
        getTvToolbarTitle().setText(getString(R.string.services));
        getToolbar().setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.arrow));
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getSearchMenuItem().setVisible(false);
        getFilterMenuItem().setVisible(false);
        getOkMenuItem().setVisible(true);
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_ok:
                //mWrappedAdapter.notifyDataSetChanged();
                for(int i = 0; i < pricesModelArrayList.size(); i++){
                    for(int j = 0; j < pricesModelArrayList.get(i).getSubPricesModels().size(); j++){
                        pricesModelArrayList.get(i).getSubPricesModels().get(j).setValue(data.get(i).second.get(j).getPriceChildValue());
                        pricesModelArrayList.get(i).getSubPricesModels().get(j).setChecked(data.get(i).second.get(j).getChecked());
                    }
                }

                Intent intent = new Intent();
                intent.putExtra(Const.filterArray, pricesModelArrayList);
                setResult(RESULT_OK, intent);
                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
