package com.app.ersultan.assistantapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.ersultan.assistantapp.CheckUserStatus;
import com.app.ersultan.assistantapp.MainActivity;
import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.helper.Preferences;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AcceptanceActivity extends AppCompatActivity {

    public static final String TAG = AcceptanceActivity.class.getSimpleName();

    @BindView(R.id.toolbarAcceptance)
    Toolbar toolbar;

    @BindView(R.id.btnAgree)
    Button btnAgree;
    @BindView(R.id.btnNotAgree)
    Button btnNotAgree;

    @BindView(R.id.relative_layout_acceptance_content)
    RelativeLayout rlContent;
    @BindView(R.id.progress_bar_acceptance)
    ProgressBar progressBar;
    @BindView(R.id.web_view_acceptance)
    WebView webView;

    private String agreementPage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acceptance);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Intent intent = getIntent();
        agreementPage = intent.getStringExtra(Const.agreementPage);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setWebChromeClient(new WebChromeClient());

        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        String data = "<style>img,iframe{display: inline;height: auto;max-width: 100%;}</style>";
        String webText = null;
        webText = agreementPage.replace("//www.","https://");
        webView.loadDataWithBaseURL(null, data + webText, mimeType, encoding, null);

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(TextView.GONE);
                rlContent.setVisibility(View.VISIBLE);
            }
        });
        Log.i(TAG, "AGREEMENT: " + agreementPage);
    }

    @OnClick(R.id.btnNotAgree)
    public void btnNotAgree(){
        deleteUser();
    }

    private void deleteUser(){
        rlContent.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        final SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);

        AndroidNetworking.post(Const.URL_DELETE_USER)
            .addHeaders(Const.accessToken, sharedPreferences.getString(Preferences.userAccessToken, null))
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        int stCode = Integer.parseInt(response.getString("statusCode").toString());
                        Log.i(TAG, "DELETE STATUS CODE: " + response.getString("statusCode"));
                        if(stCode >= 200 && stCode < 300){
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean(Preferences.logged, false);
                            editor.commit();

                            startActivity(new Intent(AcceptanceActivity.this, AuthorizationActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    try {
                        JSONObject response = new JSONObject(anError.getErrorBody());
                        if(response.getString("statusCode").equals("401")){

                            Toast.makeText(AcceptanceActivity.this, getString(R.string.session_error), Toast.LENGTH_LONG);

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Preferences.userAccessToken, null);
                            editor.putBoolean(Preferences.logged, false);
                            editor.commit();

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    startActivity(new Intent(AcceptanceActivity.this, CheckUserStatus.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                }
                            }, 3500);
                        } else if(response.getString("statusCode").equals("500")){
                            Toast.makeText(AcceptanceActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        } else if(response.getString("statusCode").equals("408")){
                            Toast.makeText(AcceptanceActivity.this, getString(R.string.timeout_error), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            });
    }

    @OnClick(R.id.btnAgree)
    public void btnAgree(){
        addFCMToken();
    }

    private void addFCMToken(){
        rlContent.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
        params.put("fcm_token", FirebaseInstanceId.getInstance().getToken());
        params.put("device_type", "2");

        final SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);

        AndroidNetworking.post(Const.URL_ADD_FCM_TOKEN)
            .addBodyParameter(params)
            .addHeaders("accessToken", sharedPreferences.getString(Preferences.userAccessToken, null))
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        int stCode = Integer.parseInt(response.getString("statusCode").toString());
                        if(stCode >= 200 && stCode < 300){
                            SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean(Preferences.logged, true);
                            editor.commit();

                            progressBar.setVisibility(View.GONE);

                            startActivity(new Intent(AcceptanceActivity.this, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    try {
                        JSONObject response = new JSONObject(anError.getErrorBody());
                        if(response.getString("statusCode").equals("401")){

                            Toast.makeText(AcceptanceActivity.this, getString(R.string.session_error), Toast.LENGTH_LONG).show();

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Preferences.userAccessToken, null);
                            editor.putBoolean(Preferences.logged, false);
                            editor.commit();

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    startActivity(new Intent(AcceptanceActivity.this, CheckUserStatus.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                }
                            }, 3500);
                        } else if(response.getString("statusCode").equals("500")){
                            Toast.makeText(AcceptanceActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        } else if(response.getString("statusCode").equals("408")){
                            Toast.makeText(AcceptanceActivity.this, getString(R.string.timeout_error), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }

                    if(progressBar.getVisibility() == View.VISIBLE){
                        progressBar.setVisibility(View.GONE);
                    }
                }
            });
    }
}
