package com.app.ersultan.assistantapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.model.SingleItemModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

/**
 * Created by ersultan on 10/20/17.
 */

public class MoreVenueAdapter extends RecyclerView.Adapter<MoreVenueAdapter.MyViewHolder> {

    public static final String TAG = MoreVenueAdapter.class.getSimpleName();
    private Context context;
    private List<SingleItemModel> venueList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageVenue, ivDiscount;
        public TextView tvReviewsCount, tvRatingCount, tvVenueType, tvVenueName, tvDiscount;

        public MyViewHolder(View itemView){
            super(itemView);
            imageVenue = itemView.findViewById(R.id.image_more_venue);
            tvReviewsCount = itemView.findViewById(R.id.text_more_reviews_count);
            tvRatingCount = itemView.findViewById(R.id.text_more_rating_count);
            tvVenueType = itemView.findViewById(R.id.text_more_venue_type);
            tvVenueName = itemView.findViewById(R.id.text_more_venue_name);
            tvDiscount = itemView.findViewById(R.id.text_discount_more_venues);
            ivDiscount = itemView.findViewById(R.id.image_discount_more_venues);
        }
    }

    public MoreVenueAdapter(Context context, List<SingleItemModel> venueList){
        this.context = context;
        this.venueList = venueList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_venue_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SingleItemModel singleItemModel = venueList.get(position);
        holder.tvVenueType.setText(singleItemModel.getType());
        holder.tvVenueName.setText(singleItemModel.getName());
        holder.tvRatingCount.setText(singleItemModel.getRating());
        holder.tvReviewsCount.setText(singleItemModel.getReviews());

        Log.i(TAG, "More venue adapter: " + singleItemModel.getDiscount());

        holder.tvDiscount.setText("-" + singleItemModel.getDiscount() + "%");
        holder.ivDiscount.setBackgroundResource(R.drawable.discount);

        if(!singleItemModel.getDiscount().equals("0")){
            holder.tvDiscount.setVisibility(View.VISIBLE);
            holder.ivDiscount.setVisibility(View.VISIBLE);
        } else {
            holder.tvDiscount.setVisibility(View.GONE);
            holder.ivDiscount.setVisibility(View.GONE);
        }

        Glide.with(context)
                .load(singleItemModel.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .centerCrop()
                .into(holder.imageVenue);
    }

    @Override
    public int getItemCount() {
        return venueList.size();
    }
}


















