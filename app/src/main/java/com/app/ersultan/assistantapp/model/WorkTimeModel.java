package com.app.ersultan.assistantapp.model;

import java.io.Serializable;

/**
 * Created by ersultan on 10/23/17.
 */

public class WorkTimeModel implements Serializable{

    public String time = "";
    public String timeId = "";
    public String checked = "";

    public WorkTimeModel(){}

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTimeId() {
        return timeId;
    }

    public void setTimeId(String timeId) {
        this.timeId = timeId;
    }
}
