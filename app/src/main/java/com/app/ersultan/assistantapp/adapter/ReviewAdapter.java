package com.app.ersultan.assistantapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.model.ReviewModel;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ersultan on 10/19/17.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder>{

    public static final String TAG = ReviewAdapter.class.getSimpleName();

    private Context context;
    private Deque<ReviewModel> reviewModelList;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvUserName, tvUserReview, tvTime;
        public View  divider;

        public ViewHolder(View view){
            super(view);
            tvUserName = view.findViewById(R.id.text_user_name);
            tvUserReview = view.findViewById(R.id.text_user_review);
            tvTime = view.findViewById(R.id.text_review_time);
            divider = view.findViewById(R.id.view_review_divider);
        }
    }

    public ReviewAdapter(Context context, Deque<ReviewModel> reviewModels){
        this.context = context;
        this.reviewModelList = reviewModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ReviewModel reviewModel = getByIndex(reviewModelList, position);
        holder.tvUserName.setText(reviewModel.getUserName());
        holder.tvUserReview.setText(reviewModel.getUserReview());
        holder.tvTime.setText(reviewModel.getReviewTime());
    }

    public static ReviewModel getByIndex(Deque deque, int index){
        List list = (LinkedList) deque;
        return (ReviewModel) list.get(index);
    }

    @Override
    public int getItemCount() {
        return reviewModelList.size();
    }
}
