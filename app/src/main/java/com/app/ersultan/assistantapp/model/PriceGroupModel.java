package com.app.ersultan.assistantapp.model;

import java.io.Serializable;

/**
 * Created by ersultan on 12/4/17.
 */

public class PriceGroupModel implements Serializable {

    private String priceGroupId = "";
    private String priceGroupTitle = "";

    public PriceGroupModel(){}

    public String getPriceGroupId() {
        return priceGroupId;
    }

    public void setPriceGroupId(String priceGroupId) {
        this.priceGroupId = priceGroupId;
    }

    public String getPriceGroupTitle() {
        return priceGroupTitle;
    }

    public void setPriceGroupTitle(String priceGroupTitle) {
        this.priceGroupTitle = priceGroupTitle;
    }
}
