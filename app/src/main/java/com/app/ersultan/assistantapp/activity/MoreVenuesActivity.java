package com.app.ersultan.assistantapp.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.ersultan.assistantapp.CheckUserStatus;
import com.app.ersultan.assistantapp.MainActivity;
import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.adapter.MoreVenueAdapter;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.helper.GridSpacingItemDecoration;
import com.app.ersultan.assistantapp.helper.Preferences;
import com.app.ersultan.assistantapp.helper.RecyclerTouchListener;
import com.app.ersultan.assistantapp.model.SingleItemModel;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import dmax.dialog.SpotsDialog;

public class MoreVenuesActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = MoreVenuesActivity.class.getSimpleName();
    @BindView(R.id.recycler_view_more_venues)
    RecyclerView recyclerView;
    private MoreVenueAdapter adapter;
    private List<SingleItemModel> venuesList = new ArrayList<>();

    @BindView(R.id.relative_layout_more_venues_content)
    RelativeLayout rlContent;
    @BindView(R.id.progress_bar_more_venues)
    ProgressBar progressBar;
    @BindView(R.id.progress_bar_more_venues_under)
    ProgressBar pbUnderRecyclerView;

    @BindView(R.id.nested_scroll_view_more_venues)
    NestedScrollView nestedScrollView;
    @BindView(R.id.swipe_refresh_layout_more_venues)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.material_search_view_more_venues)
    MaterialSearchView searchView;
    @BindView(R.id.text_request_result_more_venues)
    TextView tvNotFound;
    private boolean allSearchDataLoaded = false;
    private int currentSearchPage = 1;
    private String typedSearch = "";
    private AlertDialog customProgressDialog = null;

    private SharedPreferences sharedPreferences = null;
    private String url = "";

    int currentPage = 1;
    boolean allVenuesShowed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_venues);
        setToolbar();

        sharedPreferences = getSharedPreferences(Preferences.shPref, 0);
        Intent intent = getIntent();
        url = Const.URL_GET_VENUES_BY_CATEGORY_ID + intent.getStringExtra(Const.venuesCategoryId);

        customProgressDialog = new SpotsDialog(this, R.style.CustomProgressDialog);
        customProgressDialog.setCancelable(false);
        swipeRefreshLayout.setOnRefreshListener(this);

        searchViewListener();
        prepareRecyclerView();
        loadVenues(String.valueOf(currentPage), typedSearch);
    }

    private void prepareRecyclerView(){
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(5), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(MoreVenuesActivity.this, DetailedItemActivity.class);
                intent.putExtra(Const.venueId, venuesList.get(position).getId());
                intent.putExtra(Const.venueType, venuesList.get(position).getType());
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) { }
        }));
        adapter = new MoreVenueAdapter(this, venuesList);
        recyclerView.setAdapter(adapter);

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if(searchView.isSearchOpen()){
                        if(!allSearchDataLoaded){
                            currentSearchPage = currentSearchPage + 1;
                            loadVenues(String.valueOf(currentSearchPage), typedSearch);
                        }
                    } else {
                        if(!allVenuesShowed){
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    currentPage = currentPage + 1;
                                    loadVenues(String.valueOf(currentPage), typedSearch);
                                }
                            }, 50);
                        }
                    }
                }
            }
        });
    }

    private void loadVenues(final String page, final String searchText){
        AndroidNetworking.get(url)
            .addHeaders("accessToken", sharedPreferences.getString(Preferences.userAccessToken, null))
            .addQueryParameter("page", String.valueOf(page))
            .addQueryParameter("search_text", searchText)
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if(response.getString("statusCode").equals("200")){
                            JSONObject result = response.getJSONObject("result");
                            JSONArray venues = result.getJSONArray("venues");
                            for(int i = 0; i < venues.length(); i++){
                                JSONObject venuesItem = venues.getJSONObject(i);
                                SingleItemModel singleItemModel = new SingleItemModel();
                                singleItemModel.setId(venuesItem.getString("id"));
                                singleItemModel.setImageURL(Const.DOMAIN_URL + venuesItem.getString("image"));
                                singleItemModel.setReviews(venuesItem.getString("reviews_count"));
                                singleItemModel.setRating(venuesItem.getString("rating"));
                                singleItemModel.setType(venuesItem.getString("category_title"));
                                singleItemModel.setName(venuesItem.getString("title"));
                                singleItemModel.setDiscount(venuesItem.getString("discount"));
                                venuesList.add(singleItemModel);
                            }

                            JSONObject details = result.getJSONObject("details");
                            int totalCount = details.getInt("count");
                            if(venues.length() == 0 || venuesList.size() == totalCount){
                                pbUnderRecyclerView.setVisibility(View.GONE);
                                allVenuesShowed = true;
                            } else {
                                pbUnderRecyclerView.setVisibility(View.VISIBLE);
                                allVenuesShowed = false;
                            }

                            if(searchView.isSearchOpen()){
                                if(venuesList.size() == 0){
                                    tvNotFound.setVisibility(View.VISIBLE);
                                } else {
                                    tvNotFound.setVisibility(View.GONE);
                                }
                            }

                            customProgressDialog.dismiss();
                            swipeRefreshLayout.setRefreshing(false);
                            adapter.notifyDataSetChanged();
                            rlContent.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    try {
                        JSONObject response = new JSONObject(anError.getErrorBody());
                        if(response.getString("statusCode").equals("401")){

                            Toast.makeText(MoreVenuesActivity.this, getString(R.string.session_error), Toast.LENGTH_LONG).show();

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Preferences.userAccessToken, null);
                            editor.putBoolean(Preferences.logged, false);
                            editor.commit();

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    startActivity(new Intent(MoreVenuesActivity.this, CheckUserStatus.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                }
                            }, 3500);
                        } else if(response.getString("statusCode").equals("500")){
                            Toast.makeText(MoreVenuesActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            rlContent.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);

                        } else if(response.getString("statusCode").equals("408")){
                            Toast.makeText(MoreVenuesActivity.this, getString(R.string.timeout_error), Toast.LENGTH_LONG).show();
                            rlContent.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            });

    }

    public void searchViewListener(){
        searchView.setHint(getString(R.string.search_hint_text));
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                setDefaultValues();
                customProgressDialog.show();
                rlContent.setVisibility(View.GONE);
                loadVenues(String.valueOf(currentSearchPage), typedSearch);
            }

            @Override
            public void onSearchViewClosed() {
                setDefaultValues();
                typedSearch = "";
                customProgressDialog.show();
                rlContent.setVisibility(View.VISIBLE);
                loadVenues(String.valueOf(currentPage), typedSearch);
            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                setDefaultValues();
                customProgressDialog.show();
                typedSearch = query;
                loadVenues(String.valueOf(currentSearchPage), typedSearch);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
    }

    private void setDefaultValues(){
        currentSearchPage = 1;
        currentPage = 1;

        venuesList.clear();
        adapter.notifyDataSetChanged();
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void setToolbar(){
        Intent intent = getIntent();
        getTvToolbarTitle().setText(intent.getStringExtra(Const.venuesCategoryName));
        getToolbar().setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.arrow));
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        searchView.setMenuItem(getSearchMenuItem());
        getFilterMenuItem().setVisible(false);
        getOkMenuItem().setVisible(false);
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public void onRefresh() {
        setDefaultValues();
        pbUnderRecyclerView.setVisibility(View.VISIBLE);
        loadVenues(String.valueOf(currentPage), typedSearch);
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }
}
