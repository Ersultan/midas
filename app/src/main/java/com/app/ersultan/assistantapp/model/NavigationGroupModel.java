package com.app.ersultan.assistantapp.model;

/**
 * Created by ersultan on 11/2/17.
 */

public class NavigationGroupModel {

    private int groupIcon = 0;
    private String groupTitle = "";
    private String groupId = "";

    public NavigationGroupModel(){}

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public int getGroupIcon() {
        return groupIcon;
    }

    public void setGroupIcon(int groupIcon) {
        this.groupIcon = groupIcon;
    }

    public String getGroupTitle() {
        return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }

}
