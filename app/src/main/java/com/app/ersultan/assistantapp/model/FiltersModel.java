package com.app.ersultan.assistantapp.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ersultan on 10/23/17.
 */

public class FiltersModel implements Serializable{

    public String filtersId = "";
    public String filtersTitle = "";
    public String isLong = "";
    public String valuesString = "";
    public String filterIsMarked = "";
    public ArrayList<ValuesOfFiltersModel> valuesOfFiltersModelArrayList = new ArrayList<>();

    public FiltersModel(){}

    public String getValuesString() {
        return valuesString;
    }

    public void setValuesString(String valuesString) {
        this.valuesString = valuesString;
    }

    public String getIsLong() {
        return isLong;
    }

    public void setIsLong(String isLong) {
        this.isLong = isLong;
    }

    public String getFiltersId() {
        return filtersId;
    }

    public void setFiltersId(String filtersId) {
        this.filtersId = filtersId;
    }

    public String getFiltersTitle() {
        return filtersTitle;
    }

    public void setFiltersTitle(String filtersTitle) {
        this.filtersTitle = filtersTitle;
    }

    public ArrayList<ValuesOfFiltersModel> getValuesOfFiltersModelArrayList() {
        return valuesOfFiltersModelArrayList;
    }

    public void setValuesOfFiltersModelArrayList(ArrayList<ValuesOfFiltersModel> valuesOfFiltersModelArrayList) {
        this.valuesOfFiltersModelArrayList = valuesOfFiltersModelArrayList;
    }

    public String getFilterIsMarked() {
        return filterIsMarked;
    }

    public void setFilterIsMarked(String filterIsMarked) {
        this.filterIsMarked = filterIsMarked;
    }
}
