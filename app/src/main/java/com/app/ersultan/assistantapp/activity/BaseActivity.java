package com.app.ersultan.assistantapp.activity;

import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.app.ersultan.assistantapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ersultan on 9/25/17.
 */

public class BaseActivity extends AppCompatActivity{

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvToolbarTitle)
    TextView tvToolbarTitle;

    private MenuItem searchMenuItem;
    private MenuItem filterMenuItem;
    private MenuItem okMenuItem;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        AndroidNetworking.initialize(getApplicationContext());
        bindViews();
    }

    protected void bindViews(){
        ButterKnife.bind(this);
        setupToolbar();
    }

    public void setContentViewWithoutInject(int layoutResID){
        super.setContentView(layoutResID);
    }

    protected void setupToolbar(){
        if(toolbar != null){
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setNavigationIcon(R.drawable.ic_navigation);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        searchMenuItem = menu.findItem(R.id.action_search);
        filterMenuItem = menu.findItem(R.id.action_filter);
        okMenuItem = menu.findItem(R.id.action_ok);
        return true;
    }

    public Toolbar getToolbar(){
        return toolbar;
    }

    public MenuItem getFilterMenuItem() { return filterMenuItem; }

    public MenuItem getSearchMenuItem() {
        return searchMenuItem;
    }

    public MenuItem getOkMenuItem() {
        return okMenuItem;
    }

    public TextView getTvToolbarTitle(){
        return tvToolbarTitle;
    }
}

