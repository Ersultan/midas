package com.app.ersultan.assistantapp.helper;

/**
 * Created by ersultan on 9/25/17.
 */

public class Preferences {

    public static final String shPref = "shPref";
    public static final String logged = "logged";
    public static final String userAccessToken = "userAccessToken";
}
