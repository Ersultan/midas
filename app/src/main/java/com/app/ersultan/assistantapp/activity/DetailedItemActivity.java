package com.app.ersultan.assistantapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.ersultan.assistantapp.CheckUserStatus;
import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.fragment.FullScreenImageFragment;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.helper.Preferences;
import com.app.ersultan.assistantapp.helper.SliderPagerAdapter;
import com.app.ersultan.assistantapp.model.FullScreenImageModel;
import com.app.ersultan.assistantapp.model.MenuChildModel;
import com.app.ersultan.assistantapp.model.MenuGroupModel;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.transitionseverywhere.TransitionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class DetailedItemActivity extends BaseActivity implements OnMapReadyCallback, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = DetailedItemActivity.class.getSimpleName();
    private String venueId = "";

    private GoogleMap googleMap;

    @BindView(R.id.progress_bar_detailed_item)
    ProgressBar progressBar;
    @BindView(R.id.rlDetailedItemContent)
    RelativeLayout rlDetailedContent;

    @BindView(R.id.view_pager_detailed_item_slider)
    ViewPager vpSlider;
    private SliderPagerAdapter sliderPagerAdapter;

    @BindView(R.id.text_detailed_item_name)
    TextView tvVenueName;
    @BindView(R.id.text_rating_count)
    TextView tvRatingCount;
    @BindView(R.id.image_rating)
    ImageView ivRatingCount;
    @BindView(R.id.text_reviews_count)
    TextView tvReviewsCount;
    @BindView(R.id.text_working_hours)
    TextView tvWorkingHours;
    @BindView(R.id.llFeaturesAndKitchen)
    LinearLayout llFeaturesAndKitchen;
    @BindView(R.id.relative_layout_menu)
    RelativeLayout rlMenuButton;
    @BindView(R.id.text_menu)
    TextView tvMenu;
    @BindView(R.id.llExtraInformation)
    LinearLayout llExtraInformation;
    @BindView(R.id.llInformation)
    LinearLayout llInformation;
    @BindView(R.id.relative_layout_reviews)
    RelativeLayout rlReviewButton;
    @BindView(R.id.text_reviews_count_in_bracket)
    TextView tvReviewsCountInBracket;
    @BindView(R.id.llMedia)
    LinearLayout llMedia;
    @BindView(R.id.view_click_on_map)
    View viewMap;

    @BindView(R.id.swipe_refresh_layout_detailed_item_content)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.relative_layout_detailed_item_discount)
    RelativeLayout rlDiscount;
    @BindView(R.id.text_detailed_item_discount)
    TextView tvDiscount;

    @BindView(R.id.image_left_slide)
    ImageView ivLeftSlide;
    @BindView(R.id.image_right_slide)
    ImageView ivRightSlide;

    private double venueLatitude = 0.0;
    private double venueLongitude = 0.0;
    private String venueAddress = "";
    private String venueName = "";

    private ArrayList<String> sliderImagesUrl = new ArrayList<>();
    private ArrayList<FullScreenImageModel> fullScreenImageModelArrayList = new ArrayList<>();

    private JSONObject menuPriceObject = new JSONObject();
    private JSONArray menuPriceArray = new JSONArray();

    private int REVIEWS_COUNT_CODE = 189;

    private ViewGroup transitionContainer = null;
    @BindView(R.id.llShowSchedule)
    LinearLayout llShowSchedule;
    @BindView(R.id.linear_layout_working_days)
    LinearLayout llWorkingHours;

    @BindView(R.id.llMonday)
    LinearLayout llMonday;
    @BindView(R.id.tvMondayTitle)
    TextView tvMondayTitle;
    @BindView(R.id.tvMondayTime)
    TextView tvMondayTime;

    @BindView(R.id.llTuesday)
    LinearLayout llTuesday;
    @BindView(R.id.tvTuesdayTitle)
    TextView tvTuesdayTitle;
    @BindView(R.id.tvTuesdayTime)
    TextView tvTuesdayTime;

    @BindView(R.id.llWednesday)
    LinearLayout llWednesday;
    @BindView(R.id.tvWednesdayTitle)
    TextView tvWednesdayTitle;
    @BindView(R.id.tvWednesdayTime)
    TextView tvWednesdayTime;

    @BindView(R.id.llThursday)
    LinearLayout llThursday;
    @BindView(R.id.tvThursdayTitle)
    TextView tvThursdayTitle;
    @BindView(R.id.tvThursdayTime)
    TextView tvThursdayTime;

    @BindView(R.id.llFriday)
    LinearLayout llFriday;
    @BindView(R.id.tvFridayTitle)
    TextView tvFridayTitle;
    @BindView(R.id.tvFridayTime)
    TextView tvFridayTime;

    @BindView(R.id.llSaturday)
    LinearLayout llSaturday;
    @BindView(R.id.tvSaturdayTitle)
    TextView tvSaturdayTitle;
    @BindView(R.id.tvSaturdayTime)
    TextView tvSaturdayTime;

    @BindView(R.id.llSunday)
    LinearLayout llSunday;
    @BindView(R.id.tvSundayTitle)
    TextView tvSundayTitle;
    @BindView(R.id.tvSundayTime)
    TextView tvSundayTime;

    private ArrayList<Pair<MenuGroupModel, List<MenuChildModel>>> data = new ArrayList<Pair<MenuGroupModel, List<MenuChildModel>>>();
    private Double serviceChargeValue = 0.0;
    private Double discountValue = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_detailed_item);
        setToolbar();

        transitionContainer = findViewById(R.id.rlDetailedItemContent);
        swipeRefreshLayout.setOnRefreshListener(this);

        Intent intent = getIntent();
        venueId = intent.getStringExtra(Const.venueId);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_detailed_item);
        mapFragment.getMapAsync(this);

        loadDetailedInformation();
    }

    private void loadDetailedInformation(){
        final SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);
        String url = Const.GET_VENUE_BY_ID + venueId;

        AndroidNetworking.get(url)
            .addHeaders("accessToken", sharedPreferences.getString(Preferences.userAccessToken, null))
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if(response.getString("statusCode").equals("200")){
                            JSONObject result = response.getJSONObject("result");
                            venueName = result.getString("title");
                            tvVenueName.setText(result.getString("title"));
                            tvRatingCount.setText(result.getString("rating"));
                            tvReviewsCount.setText(result.getString("reviews_count"));
                            ivRatingCount.setImageResource(getRatingImage(result.getString("rating")));

                            String today = getString(R.string.today_with_dots);
                            SpannableStringBuilder sb = new SpannableStringBuilder(today);
                            sb.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, today.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            sb.append(" ");
                            sb.append(result.getString("work_time_today"));
                            tvWorkingHours.setText(sb);

                            JSONArray weekTimesArr = result.getJSONArray("week_times");
                            for(int i = 0; i < weekTimesArr.length(); i++){
                                JSONObject weekTime = weekTimesArr.getJSONObject(i);
                                String time = "";
                                boolean isToday;

                                if(weekTime.getString("is_today").equals("1")){
                                    isToday = true;
                                } else {
                                    isToday = false;
                                }

                                if(weekTime.getString("other_time").equals("")){
                                    time = weekTime.getString("open_time") + "\n" + weekTime.getString("close_time");
                                } else {
                                    time = weekTime.getString("other_time");
                                }

                                setSchedule(i, time, isToday);
                            }

                            // set discount
                            String discount = result.getString("discount");
                            if(!discount.equals("0")){
                                rlDiscount.setVisibility(View.VISIBLE);
                                tvDiscount.setText("-" + discount + "%");
                            } else {
                                rlDiscount.setVisibility(View.GONE);
                            }

                            // Filling filters
                            llFeaturesAndKitchen.removeAllViews();
                            JSONArray filters = result.getJSONArray("filters");
                            if(filters.length() > 0){
                                for(int i = 0; i < filters.length(); i++){
                                    JSONObject filterItem = filters.getJSONObject(i);

                                    LinearLayout.LayoutParams featuresAndKitchenLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    featuresAndKitchenLayoutParams.topMargin = -2;

                                    LinearLayout llFilterInformation = new LinearLayout(DetailedItemActivity.this);
                                    llFilterInformation.setLayoutParams(featuresAndKitchenLayoutParams);
                                    llFilterInformation.setOrientation(LinearLayout.VERTICAL);
                                    llFeaturesAndKitchen.addView(llFilterInformation);

                                    TextView filterTitle = new TextView(DetailedItemActivity.this);
                                    filterTitle.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                    filterTitle.setText(filterItem.getString("filter_title") + ":");

                                    if(filterItem.has("filter_is_marked")){
                                        if(filterItem.getString("filter_is_marked").equals("1")){
                                            filterTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                                            llFilterInformation.setBackground(getCorrectIsMarkedDrawable(filters.length(), i));
                                        } else {
                                            filterTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimary));
                                            llFilterInformation.setBackground(getCorrectIsNotMarkedDrawable(filters.length(), i));
                                        }
                                    } else {
                                        filterTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimary));
                                        llFilterInformation.setBackground(getCorrectIsNotMarkedDrawable(filters.length(), i));
                                    }

                                    filterTitle.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                                    llFilterInformation.addView(filterTitle);

                                    JSONArray values = filterItem.getJSONArray("values");
                                    if(values.length() > 0){
                                        String valuesArr = "";
                                        for(int j = 0; j < values.length(); j++){
                                            valuesArr += values.get(j).toString() + ", ";
                                        }

                                        valuesArr = valuesArr.substring(0, valuesArr.length() - 2);
                                        TextView valuesText = new TextView(DetailedItemActivity.this);
                                        valuesText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                        valuesText.setText(valuesArr);
                                        if(filterItem.has("filter_is_marked")){
                                            if(filterItem.getString("filter_is_marked").equals("1")){
                                                valuesText.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                                            } else {
                                                valuesText.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                                            }
                                        } else {
                                            valuesText.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                                        }

                                        llFilterInformation.addView(valuesText);
                                    }

                                }
                            } else {
                                llFeaturesAndKitchen.setVisibility(View.GONE);
                            }

                            // Filling extra information
                            llExtraInformation.removeAllViews();
                            JSONArray extraInformationArray = result.getJSONArray("extra_information");
                            if(extraInformationArray.length() > 0){
                                for(int i = 0; i < extraInformationArray.length(); i++){
                                    JSONObject extraInformation = extraInformationArray.getJSONObject(i);
                                    String title = extraInformation.getString("title");
                                    String text = extraInformation.getString("text");

                                    LinearLayout.LayoutParams extraInformationLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    extraInformationLayoutParams.topMargin = -2;

                                    LinearLayout linearLayout = new LinearLayout(DetailedItemActivity.this);
                                    linearLayout.setLayoutParams(extraInformationLayoutParams);
                                    linearLayout.setOrientation(LinearLayout.VERTICAL);
                                    llExtraInformation.addView(linearLayout);

                                    TextView tvTitle = new TextView(DetailedItemActivity.this);
                                    tvTitle.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                    tvTitle.setText(title);
                                    if(extraInformation.has("is_marked")){
                                        if(extraInformation.getString("is_marked").equals("1")){
                                            tvTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                                            linearLayout.setBackground(getCorrectIsMarkedDrawable(extraInformationArray.length(), i));
                                        } else {
                                            tvTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimary));
                                            linearLayout.setBackground(getCorrectIsNotMarkedDrawable(extraInformationArray.length(), i));
                                        }
                                    } else {
                                        tvTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimary));
                                        linearLayout.setBackground(getCorrectIsNotMarkedDrawable(extraInformationArray.length(), i));
                                    }
                                    tvTitle.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                                    linearLayout.addView(tvTitle);

                                    TextView tvText = new TextView(DetailedItemActivity.this);
                                    tvText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                    tvText.setText(text);
                                    if(extraInformation.has("is_marked")){
                                        if(extraInformation.getString("is_marked").equals("1")){
                                            tvText.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                                        } else {
                                            tvText.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                                        }
                                    } else {
                                        tvText.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                                    }

                                    linearLayout.addView(tvText);
                                }
                            } else {
                                llExtraInformation.setVisibility(View.GONE);
                            }

                            // Filling information
                            llInformation.removeAllViews();
                            int infoCounter = 0;
                            JSONArray informationJSArray = result.getJSONArray("information");
                            for(int i = 0 ; i < informationJSArray.length() + 2; i++){
                                JSONObject informationItem = new JSONObject();
                                if(informationJSArray.length() > 0){
                                    informationItem = informationJSArray.getJSONObject(infoCounter);
                                }

                                LinearLayout.LayoutParams informationLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                informationLayoutParams.topMargin = -2;

                                LinearLayout llAddress = new LinearLayout(DetailedItemActivity.this);
                                llAddress.setLayoutParams(informationLayoutParams);
                                llAddress.setOrientation(LinearLayout.HORIZONTAL);

                                LinearLayout llAddressFirstColumn = new LinearLayout(DetailedItemActivity.this);
                                llAddressFirstColumn.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
                                llAddress.addView(llAddressFirstColumn);

                                final TextView tvKeyText = new TextView(DetailedItemActivity.this);
                                tvKeyText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                if(informationItem.has("is_marked")){
                                    if(informationItem.getString("is_marked").equals("1") && i != 0 && i != 1){
                                        tvKeyText.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                                        llAddress.setBackground(getInfoCorrectIsMarkedDrawable(informationJSArray.length() + 2, i));
                                    } else {
                                        tvKeyText.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimary));
                                        llAddress.setBackground(getInfoCorrectIsNotMarkedDrawable(informationJSArray.length() + 2, i));
                                    }
                                } else {
                                    tvKeyText.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimary));
                                    llAddress.setBackground(getInfoCorrectIsNotMarkedDrawable(informationJSArray.length() + 2, i));
                                }

                                if(i == 0){
                                    tvKeyText.setText(R.string.address_with_dots);
                                } else if(i == 1){
                                    tvKeyText.setText(R.string.phone_with_dots);
                                } else {
                                    tvKeyText.setText(informationItem.getString("information_title") + ":");
                                }
                                tvKeyText.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                                tvKeyText.setPadding(20, 20, 20, 20);
                                llAddressFirstColumn.addView(tvKeyText);

                                LinearLayout.LayoutParams verticalDividerParams = new LinearLayout.LayoutParams(2, ViewGroup.LayoutParams.MATCH_PARENT);
                                //verticalDividerParams.setMargins(20, 0, 20, 0);

                                View firstDivider = new View(DetailedItemActivity.this);
                                firstDivider.setLayoutParams(verticalDividerParams);
                                firstDivider.setBackgroundColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.divider));
                                llAddress.addView(firstDivider);

                                LinearLayout llAddressSecondColumn = new LinearLayout(DetailedItemActivity.this);
                                llAddressSecondColumn.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.4f));
                                llAddress.addView(llAddressSecondColumn);

                                final TextView tvKeyValue = new TextView(DetailedItemActivity.this);
                                tvKeyValue.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                if(informationItem.has("is_marked")){
                                    if(informationItem.getString("is_marked").equals("1") && i != 0 && i != 1){
                                        tvKeyValue.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                                    } else {
                                        tvKeyValue.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimary));
                                    }
                                } else {
                                    tvKeyValue.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimary));
                                }

                                if(i == 0){
                                    tvKeyValue.setText(result.getString("address"));
                                } else if(i == 1){
                                    JSONArray phones = result.getJSONArray("phone");
                                    String phonesArr = "";
                                    for(int j = 0; j < phones.length(); j++){
                                        phonesArr += phones.get(j).toString();
                                        if(j != phones.length() - 1){
                                            phonesArr += "\n";
                                        }
                                    }
                                    tvKeyValue.setText(phonesArr);
                                    tvKeyValue.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.call_color));
                                    tvKeyValue.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            String[] separatedPhoneNumbers = tvKeyValue.getText().toString().split("\n");

                                            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(DetailedItemActivity.this);
                                            LayoutInflater inflater = getLayoutInflater();
                                            View dialogView = inflater.inflate(R.layout.custom_dialog_with_list_view, null);
                                            dialogBuilder.setView(dialogView);

                                            final ListView listView = dialogView.findViewById(R.id.list_view_phone_dialog);
                                            ArrayAdapter<String> adapter = new ArrayAdapter<>(DetailedItemActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, separatedPhoneNumbers);
                                            listView.setAdapter(adapter);

                                            dialogBuilder.setTitle(getString(R.string.choose_phone_number));
                                            dialogBuilder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });

                                            final AlertDialog alertDialog = dialogBuilder.create();
                                            Window window = alertDialog.getWindow();
                                            window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                            window.setGravity(Gravity.CENTER); // set alert dialog in center

                                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                                    String selectedPhoneNumber = (String) listView.getItemAtPosition(i);
                                                    Intent intent = new Intent(Intent.ACTION_DIAL);
                                                    intent.setData(Uri.parse("tel:" + selectedPhoneNumber));
                                                    startActivity(intent);
                                                    alertDialog.dismiss();
                                                }
                                            });

                                            alertDialog.show();
                                        }
                                    });

                                } else {
                                    if(informationItem.getString("end_string").equals(""))
                                        tvKeyValue.setText(informationItem.getString("value"));
                                    else
                                        tvKeyValue.setText(informationItem.getString("value") + " " + informationItem.getString("end_string"));
                                    infoCounter++;
                                }

                                tvKeyValue.setPadding(20, 20, 20, 20);
                                llAddressSecondColumn.addView(tvKeyValue);

//                                if(i != (informationJSArray.length() + 3) - 1 && i != 0){
//                                    View divider = new View(DetailedItemActivity.this);
//                                    divider.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2));
//                                    divider.setBackgroundColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
//                                    llInformation.addView(divider);
//                                }

                                llInformation.addView(llAddress);
                            }

                            // Filling medias
                            llMedia.removeAllViews();
                            JSONArray medias = result.getJSONArray("discount_links");
                            if(medias.length() > 0){
                                for(int i = 0; i < medias.length(); i++){
                                    final TextView tvMediaValue = new TextView(DetailedItemActivity.this);
                                    tvMediaValue.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                    tvMediaValue.setPadding(20, 20, 20, 20);
                                    tvMediaValue.setText(medias.get(i).toString());
                                    tvMediaValue.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.link_color));
                                    llMedia.addView(tvMediaValue);

                                    tvMediaValue.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            String url = tvMediaValue.getText().toString();
                                            Intent browserIntent = null;
                                            if(url.startsWith("http://") || url.startsWith("https://"))
                                                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tvMediaValue.getText().toString()));
                                            else
                                                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + tvMediaValue.getText().toString()));
                                            startActivity(browserIntent);
                                        }
                                    });

                                    if(i != medias.length() - 1){
                                        View divider = new View(DetailedItemActivity.this);
                                        divider.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2));
                                        divider.setBackgroundColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                                        llMedia.addView(divider);
                                    }
                                }
                            } else {
                                llMedia.setVisibility(View.GONE);
                            }

                            // set latitude and longitude
                            venueLatitude = result.getDouble("latitude");
                            venueLongitude = result.getDouble("longitude");
                            venueAddress = result.getString("address");

                            googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(venueLatitude, venueLongitude))
                                .icon(BitmapDescriptorFactory.fromBitmap(resizeBitmap("marker", 60, 60))));

                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(venueLatitude, venueLongitude), 13.0f));

                            // reviews
                            tvReviewsCountInBracket.setText("Отзывы (" + result.getString("reviews_count") + ")");

                            // set slider
                            sliderPagerAdapter = new SliderPagerAdapter();
                            vpSlider.setAdapter(sliderPagerAdapter);
                            sliderImagesUrl.clear();
                            fullScreenImageModelArrayList.clear();
                            sliderImagesUrl.add(Const.DOMAIN_URL + result.getString("image"));
                            JSONArray mediasUrl = result.getJSONArray("medias");
                            for(int i = 0; i < mediasUrl.length(); i++){
                                sliderImagesUrl.add(Const.DOMAIN_URL + mediasUrl.get(i).toString());
                            }

                            LayoutInflater inflater = getLayoutInflater();
                            for(int i = 0; i < sliderImagesUrl.size(); i++){
                                FrameLayout frameLayout = (FrameLayout) inflater.inflate(R.layout.slider_pager_item, null, false);
                                sliderPagerAdapter.addView(frameLayout, i);
                                sliderPagerAdapter.notifyDataSetChanged();
                                View root = sliderPagerAdapter.getView(i);

                                ImageView imageView = root.findViewById(R.id.image_slider_pager);
                                if (!DetailedItemActivity.this.isFinishing()){
                                    Glide.with(DetailedItemActivity.this)
                                            .load(sliderImagesUrl.get(i))
                                            .crossFade()
                                            .into(imageView);
                                }
                            }

                            // set full screen image
                            for(int i = 0; i < sliderImagesUrl.size(); i++){
                                FullScreenImageModel fullScreenImageModel = new FullScreenImageModel();
                                fullScreenImageModel.setImage(sliderImagesUrl.get(i));
                                fullScreenImageModelArrayList.add(fullScreenImageModel);
                            }

                            viewPagerClickListener();

                            // filling menu
                            if(result.has("menue_price")){
                                Object menuObject = result.get("menue_price");
                                if(menuObject instanceof JSONObject){
                                    menuPriceObject = result.getJSONObject("menue_price");
                                    tvMenu.setText(menuPriceObject.getString("menu_price_title"));
                                    JSONArray price = menuPriceObject.getJSONArray("price");
                                    if(price.length() == 0){
                                        rlMenuButton.setVisibility(View.GONE);
                                    }
                                } else if(menuObject instanceof JSONArray){
                                    rlMenuButton.setVisibility(View.GONE);
                                }
                            } else {
                                rlMenuButton.setVisibility(View.GONE);
                            }

                            discountValue = Double.parseDouble(result.getString("discount"));
                            serviceChargeValue = Double.parseDouble(result.getString("service_charge"));

                            swipeRefreshLayout.setRefreshing(false);
                            progressBar.setVisibility(View.GONE);
                            rlDetailedContent.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    try {
                        JSONObject response = new JSONObject(anError.getErrorBody());
                        if(response.getString("statusCode").equals("401")){

                            Toast.makeText(DetailedItemActivity.this, getString(R.string.session_error), Toast.LENGTH_LONG).show();

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Preferences.userAccessToken, null);
                            editor.putBoolean(Preferences.logged, false);
                            editor.commit();

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    startActivity(new Intent(DetailedItemActivity.this, CheckUserStatus.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                }
                            }, 3500);
                        } else if(response.getString("statusCode").equals("500")){
                            Toast.makeText(DetailedItemActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            rlDetailedContent.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);

                        } else if(response.getString("statusCode").equals("408")){
                            Toast.makeText(DetailedItemActivity.this, getString(R.string.timeout_error), Toast.LENGTH_LONG).show();
                            rlDetailedContent.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            });
    }

    private Drawable getCorrectIsMarkedDrawable(int length, int i){
        Drawable drawable = null;

        if(length == 1){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.filter_is_marked_no_item);
        } else if(i == 0){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.filter_is_marked_first_item);
        } else if(i > 0 && i != length - 1){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.filter_is_marked_middle_item);
        } else if(i == length - 1){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.filter_is_marked_last_item);
        }

        return drawable;
    }

    private Drawable getCorrectIsNotMarkedDrawable(int length, int i){
        Drawable drawable = null;

        if(length == 1){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.filter_is_not_marked_no_item);
        } else if(i == 0){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.filter_is_not_marked_first_item);
        } else if(i > 0 && i != length - 1){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.filter_is_not_marked_middle_item);
        } else if(i == length - 1){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.filter_is_not_marked_last_item);
        }

        return drawable;
    }

    private Drawable getInfoCorrectIsMarkedDrawable(int length, int i){
        Drawable drawable = null;

        if(length == 1){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.info_filter_is_marked_no_item);
        } else if(i == 0){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.info_filter_is_marked_first_item);
        } else if(i > 0 && i != length - 1){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.info_filter_is_marked_middle_item);
        } else if(i == length - 1){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.info_filter_is_marked_last_item);
        }

        return drawable;
    }

    private Drawable getInfoCorrectIsNotMarkedDrawable(int length, int i){
        Drawable drawable = null;

        if(length == 1){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.info_filter_is_not_marked_no_item);
        } else if(i == 0){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.info_filter_is_not_marked_first_item);
        } else if(i > 0 && i != length - 1){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.info_filter_is_not_marked_middle_item);
        } else if(i == length - 1){
            drawable = ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.info_filter_is_not_marked_last_item);
        }

        return drawable;
    }

    @OnClick(R.id.relative_layout_menu)
    public void rlMenu(){
        if(menuPriceObject != null){
            Intent intent = new Intent(DetailedItemActivity.this, MenuActivity.class);
            intent.putExtra(Const.menuPrice, menuPriceObject.toString());
            intent.putExtra(Const.menuDiscount, discountValue);
            intent.putExtra(Const.menuServiceCharge, serviceChargeValue);
            intent.putExtra(Const.venueName, venueName);
            startActivity(intent);
        }
    }

    @OnClick(R.id.relative_layout_reviews)
    public void reviewsButton(){
        Intent intent = new Intent(DetailedItemActivity.this, ReviewsActivity.class);
        intent.putExtra(Const.venueId, venueId);
        intent.putExtra(Const.venueName, venueName);
        startActivityForResult(intent, REVIEWS_COUNT_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REVIEWS_COUNT_CODE){
            if(resultCode == RESULT_OK){
                if(!data.getStringExtra(Const.reviewsCount).equals("")){
                    tvReviewsCount.setText(data.getStringExtra(Const.reviewsCount));
                    tvReviewsCountInBracket.setText("Отзывы (" + data.getStringExtra(Const.reviewsCount) + ")");
                }
            }
        }
    }

    public Bitmap resizeBitmap(String drawableName, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier(drawableName, "drawable", getPackageName()));
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    private void viewPagerClickListener(){
        for(int i = 0; i < sliderPagerAdapter.getCount(); i++){
            sliderPagerAdapter.getView(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("images", fullScreenImageModelArrayList);
                    bundle.putInt("position", sliderPagerAdapter.getItemPosition(view));

                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    try {
                        FullScreenImageFragment newFragment = FullScreenImageFragment.class.newInstance();
                        newFragment.setArguments(bundle);
                        newFragment.show(ft, "slideshow");
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @OnClick(R.id.image_left_slide)
    public void leftSlide(){
        vpSlider.setCurrentItem(vpSlider.getCurrentItem() - 1);
    }

    @OnClick(R.id.image_right_slide)
    public void rightSlide(){
        vpSlider.setCurrentItem(vpSlider.getCurrentItem() + 1);
    }

    @OnClick(R.id.view_click_on_map)
    public void showOnMap(){
        startActivity(new Intent(DetailedItemActivity.this, VenueOnMapActivity.class)
            .putExtra(Const.venueLatitude, venueLatitude)
            .putExtra(Const.venueLongitude, venueLongitude)
            .putExtra(Const.venueAddress, venueAddress)
            .putExtra(Const.venueName, venueName));
    }

    @OnClick(R.id.llShowSchedule)
    public void workingHours(){
        if(llWorkingHours.getVisibility() == View.VISIBLE){
            TransitionManager.beginDelayedTransition(transitionContainer);
            llWorkingHours.setVisibility(View.GONE);
        } else {
            TransitionManager.beginDelayedTransition(transitionContainer);
            llWorkingHours.setVisibility(View.VISIBLE);
        }
    }

    private void setSchedule(int i, String time, boolean isToday){
        switch (i){
            case 0:
                tvMondayTitle.setText(getString(R.string.monday));
                tvMondayTime.setText(time);

                if(isToday){
                    llMonday.setBackground(ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.is_today));
                    tvMondayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                    tvMondayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                } else {
                    tvMondayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                    tvMondayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                }
                break;
            case 1:
                tvTuesdayTitle.setText(getString(R.string.tuesday));
                tvTuesdayTime.setText(time);

                if(isToday){
                    llTuesday.setBackground(ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.is_today));
                    tvTuesdayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                    tvTuesdayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                } else {
                    tvTuesdayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                    tvTuesdayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                }
                break;
            case 2:
                tvWednesdayTitle.setText(getString(R.string.wednesday));
                tvWednesdayTime.setText(time);

                if(isToday){
                    llWednesday.setBackground(ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.is_today));
                    tvWednesdayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                    tvWednesdayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                } else {
                    tvWednesdayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                    tvWednesdayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                }
                break;
            case 3:
                tvThursdayTitle.setText(getString(R.string.thursday));
                tvThursdayTime.setText(time);

                if(isToday){
                    llThursday.setBackground(ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.is_today));
                    tvThursdayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                    tvThursdayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                } else {
                    tvThursdayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                    tvThursdayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                }
                break;
            case 4:
                tvFridayTitle.setText(getString(R.string.friday));
                tvFridayTime.setText(time);

                if(isToday){
                    llFriday.setBackground(ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.is_today));
                    tvFridayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                    tvFridayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                } else {
                    tvFridayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                    tvFridayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                }
                break;
            case 5:
                tvSaturdayTitle.setText(getString(R.string.saturday));
                tvSaturdayTime.setText(time);

                if(isToday){
                    llSaturday.setBackground(ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.is_today));
                    tvSaturdayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                    tvSaturdayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                } else {
                    tvSaturdayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                    tvSaturdayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                }
                break;
            case 6:
                tvSundayTitle.setText(getString(R.string.sunday));
                tvSundayTime.setText(time);

                if(isToday){
                    llSunday.setBackground(ContextCompat.getDrawable(DetailedItemActivity.this, R.drawable.is_today));
                    tvSundayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                    tvSundayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.white));
                } else {
                    tvSundayTitle.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                    tvSundayTime.setTextColor(ContextCompat.getColor(DetailedItemActivity.this, R.color.colorPrimaryDark));
                }
                break;
        }
    }

    public void setToolbar(){
        Intent intent = getIntent();
        getTvToolbarTitle().setText(intent.getStringExtra(Const.venueType));
        getToolbar().setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.arrow));
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private int getRatingImage(String rating){
        switch (rating){
            case "1":
                return R.drawable.rating1;
            case "2":
                return R.drawable.rating2;
            case "3":
                return R.drawable.rating3;
            case "4":
                return R.drawable.rating4;
            case "5":
                return R.drawable.rating5;
            default:
                return R.drawable.rating1;
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getSearchMenuItem().setVisible(false);
        getFilterMenuItem().setVisible(false);
        getOkMenuItem().setVisible(false);
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
    }

    @Override
    public void onRefresh() {
        if(progressBar.getVisibility() == View.GONE){
            loadDetailedInformation();
        }
    }
}
