package com.app.ersultan.assistantapp.model;

import java.io.Serializable;

/**
 * Created by ersultan on 10/23/17.
 */

public class InformationModel implements Serializable{

    public String informationId = "";
    public String InformationTitle = "";
    public String minValue = "";
    public String maxValue = "";
    public String value = "";
    public String description = "";
    public String endString = "";

    public InformationModel(){}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInformationId() {
        return informationId;
    }

    public void setInformationId(String informationId) {
        this.informationId = informationId;
    }

    public String getInformationTitle() {
        return InformationTitle;
    }

    public void setInformationTitle(String informationTitle) {
        InformationTitle = informationTitle;
    }

    public String getEndString() {
        return endString;
    }

    public void setEndString(String endString) {
        this.endString = endString;
    }

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
