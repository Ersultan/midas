package com.app.ersultan.assistantapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.activity.MoreVenuesActivity;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.model.SectionDataModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

/**
 * Created by ersultan on 9/28/17.
 */

public class RecyclerViewDataAdapter extends RecyclerView.Adapter<RecyclerViewDataAdapter.ItemRowHolder>{

    private ArrayList<SectionDataModel> dataList;
    private Context mContext;

    public RecyclerViewDataAdapter(Context context, ArrayList<SectionDataModel> list){
        this.mContext = context;
        this.dataList = list;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.section_item, null);
        return new ItemRowHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemRowHolder holder, final int position) {
        final String sectionName = dataList.get(position).getHeaderTitle();

        ArrayList singleSectionItems = dataList.get(position).getAllItemsInSection();
        holder.sectionTitle.setText(sectionName);
        SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(mContext, singleSectionItems);
        holder.recyclerViewList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        holder.recyclerViewList.setNestedScrollingEnabled(false);
        holder.recyclerViewList.setAdapter(itemListDataAdapter);

        holder.llMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            mContext.startActivity(new Intent(mContext, MoreVenuesActivity.class)
                .putExtra(Const.venuesCategoryId, dataList.get(position).getCategoryId())
                .putExtra(Const.venuesCategoryName, sectionName)
            );
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView sectionTitle;
        protected RecyclerView recyclerViewList;
        protected LinearLayout llMore;

        public ItemRowHolder(View view){
            super(view);
            this.sectionTitle = view.findViewById(R.id.tvSectionTitle);
            this.recyclerViewList = view.findViewById(R.id.recycler_view_single_item_list);
            this.llMore = view.findViewById(R.id.llMore);
        }

    }

}
