package com.app.ersultan.assistantapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.adapter.ExpandableMenuAdapter;
import com.app.ersultan.assistantapp.adapter.ExpandableNavigationAdapter;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.model.MenuChildModel;
import com.app.ersultan.assistantapp.model.MenuGroupModel;
import com.app.ersultan.assistantapp.model.NavigationChildModel;
import com.app.ersultan.assistantapp.model.NavigationGroupModel;
import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MenuActivity extends BaseActivity {

    private static final String TAG = MenuActivity.class.getSimpleName();

    @BindView(R.id.recycler_view_menu)
    RecyclerView recyclerView;

    private List<Pair<MenuGroupModel, List<MenuChildModel>>> data = new LinkedList<>();
    private List<Pair<MenuGroupModel, List<MenuChildModel>>> changedData = new LinkedList<>();

    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewExpandableItemManager expMgr;

    @BindView(R.id.service_value)
    TextView tvServiceValue;
    @BindView(R.id.text_discount_value)
    TextView tvDiscountValue;
    @BindView(R.id.text_sum_value)
    TextView tvSumValue;
    @BindView(R.id.button_calculate)
    Button btnCalculate;

    private Double serviceCharge = 0.0;
    private Double discount = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setToolbar();

        Intent intent = getIntent();
        discount = intent.getDoubleExtra(Const.menuDiscount, 0.0);
        serviceCharge = intent.getDoubleExtra(Const.menuServiceCharge, 0.0);

        tvDiscountValue.setText("-" + String.valueOf(discount) + "%");
        tvServiceValue.setText("+" + String.valueOf(serviceCharge) + "%");

        setMenuList();
    }

    private void prepareRecyclerView(){
        mLayoutManager = new LinearLayoutManager(this);

        // Setup expandable feature and RecyclerView
        expMgr = new RecyclerViewExpandableItemManager(null);

        //adapter
        final ExpandableMenuAdapter myItemAdapter = new ExpandableMenuAdapter(MenuActivity.this, expMgr, data);
        mWrappedAdapter = expMgr.createWrappedAdapter(myItemAdapter);       // wrap for expanding

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();

        // Change animations are enabled by default since support-v7-recyclerview v22.
        // Need to disable them when using animation indicator.
        animator.setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mWrappedAdapter);
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setItemAnimator(animator);
        // NOTE: need to disable change animations to ripple effect work properly
        //((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

        expMgr.attachRecyclerView(recyclerView);
    }

    private void setMenuList(){
        try {
            Intent intent = getIntent();

            JSONObject result = new JSONObject(intent.getStringExtra(Const.menuPrice));

            JSONArray price = result.getJSONArray("price");
            data = new LinkedList<>();

            for(int i = 0; i < price.length(); i++){
                JSONObject menuObject = price.getJSONObject(i);

                final MenuGroupModel groupModel = new MenuGroupModel();
                groupModel.setMenuGroupId(menuObject.getString("id"));
                groupModel.setMenuGroupTitle(menuObject.getString("title"));

                final List<MenuChildModel> childModelList = new ArrayList<>();
                JSONArray values = menuObject.getJSONArray("values");
                for(int j = 0; j < values.length(); j++){
                    JSONObject subMenuObject = values.getJSONObject(j);
                    MenuChildModel childModel = new MenuChildModel();
                    childModel.setMenuChildTitle(subMenuObject.getString("price_value_title"));
                    childModel.setMenuChildPrice(subMenuObject.getString("price_value"));
                    childModel.setMenuChildId(String.valueOf(j));
                    childModel.setChecked(false);
                    if(j != values.length() - 1)
                        childModel.setChildPosition("first or middle");
                    else if(j == values.length() - 1)
                        childModel.setChildPosition("last");

                    childModelList.add(childModel);
                }

                data.add(new Pair<>(groupModel, childModelList));
            }

            prepareRecyclerView();
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getSearchMenuItem().setVisible(false);
        getFilterMenuItem().setVisible(false);
        getOkMenuItem().setVisible(false);
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public void setToolbar(){
        try {
            Intent intent = getIntent();
            JSONObject menuObject = new JSONObject(intent.getStringExtra(Const.menuPrice));
            String venueName = " «" + intent.getStringExtra(Const.venueName) + "»";
            String menuType = menuObject.getString("menu_price_title");

            getTvToolbarTitle().setText(menuType + venueName);
        } catch (JSONException e){
            e.printStackTrace();
        }

        getToolbar().setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.arrow));
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @OnClick(R.id.button_calculate)
    public void btnCalculate(){
        Double sum = 0.0;

        for(int i = 0; i < data.size(); i++){
            for(int j = 0; j < data.get(i).second.size(); j++){
                if(data.get(i).second.get(j).isChecked()){
                    Double price = Double.parseDouble(data.get(i).second.get(j).getMenuChildPrice().replaceAll(" тг", ""));
                    sum += price;
                }
            }
        }

        sum -= discount / 100 * sum;
        sum += serviceCharge / 100 * sum;
        tvSumValue.setText(String.valueOf(sum) + " тг.");

        Log.i(TAG, "discount: " + discount);
        Log.i(TAG, "service charge: " + serviceCharge);
        Log.i(TAG, "sum: " + sum);

    }

}
