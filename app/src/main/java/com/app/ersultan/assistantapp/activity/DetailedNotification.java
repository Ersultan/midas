package com.app.ersultan.assistantapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.ersultan.assistantapp.CheckUserStatus;
import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.helper.Preferences;
import com.app.ersultan.assistantapp.model.NotificationModel;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;

public class DetailedNotification extends BaseActivity {

    public static final String TAG = DetailedNotification.class.getSimpleName();
    @BindView(R.id.relative_layout_detailed_notification_content)
    RelativeLayout rlContent;
    @BindView(R.id.text_detailed_notification_title)
    TextView tvTitle;
    @BindView(R.id.text_detailed_notification_date)
    TextView tvDate;

    @BindView(R.id.web_view)
    WebView webView;
    @BindView(R.id.progress_bar_detailed_notification)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_notification);
        setToolbar();

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setWebChromeClient(new WebChromeClient());

        getNotificationById();
    }

    public void getNotificationById(){
        Intent intent = getIntent();
        String url = Const.URL_GET_NOTIFICATION_BY_ID + intent.getStringExtra(Const.notificationId);

        final SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);

        AndroidNetworking.get(url)
                .addHeaders(Const.accessToken, sharedPreferences.getString(Preferences.userAccessToken, null))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("statusCode").equals("200")){
                                final JSONObject result = response.getJSONObject("result");
                                tvTitle.setText(result.getString("title"));
                                tvDate.setText(result.getString("date"));

                                final String mimeType = "text/html";
                                final String encoding = "UTF-8";
                                String data = "<style>img,iframe{display: inline;height: auto;max-width: 100%;}</style>";
                                String webText = null;
                                webText = result.getString("text").replace("//www.","https://");
                                webView.loadDataWithBaseURL(null, data + webText, mimeType, encoding, null);

//                                webView.setWebViewClient(new WebViewClient(){
//                                    @Override
//                                    public void onPageFinished(WebView view, String url) {
//                                        rlContent.setVisibility(View.VISIBLE);
//                                        progressBar.setVisibility(View.GONE);
//                                    }
//                                });

                                rlContent.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject response = new JSONObject(anError.getErrorBody());
                            if(response.getString("statusCode").equals("401")){

                                Toast.makeText(DetailedNotification.this, getString(R.string.session_error), Toast.LENGTH_LONG).show();

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Preferences.userAccessToken, null);
                                editor.putBoolean(Preferences.logged, false);
                                editor.commit();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(DetailedNotification.this, CheckUserStatus.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    }
                                }, 3500);
                            } else if(response.getString("statusCode").equals("500")){
                                Toast.makeText(DetailedNotification.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                                rlContent.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);

                            } else if(response.getString("statusCode").equals("408")){
                                Toast.makeText(DetailedNotification.this, getString(R.string.timeout_error), Toast.LENGTH_LONG).show();
                                rlContent.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);

                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                });

    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        boolean fromNActivity = intent.getBooleanExtra(Const.fromNotificationActivity, false);
        if(fromNActivity){
            startActivity(new Intent(DetailedNotification.this, NotificationsActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            super.onBackPressed();
        } else {
            startActivity(new Intent(DetailedNotification.this, NotificationsActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
    }

    private void setToolbar(){
        getTvToolbarTitle().setText(R.string.notifications);
        getToolbar().setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.arrow));
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getSearchMenuItem().setVisible(false);
        getFilterMenuItem().setVisible(false);
        getOkMenuItem().setVisible(false);
        super.onPrepareOptionsMenu(menu);
        return true;
    }
}
