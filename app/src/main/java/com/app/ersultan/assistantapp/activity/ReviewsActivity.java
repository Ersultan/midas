package com.app.ersultan.assistantapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.ersultan.assistantapp.CheckUserStatus;
import com.app.ersultan.assistantapp.MainActivity;
import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.adapter.ReviewAdapter;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.helper.Preferences;
import com.app.ersultan.assistantapp.model.ReviewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class ReviewsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = ReviewsActivity.class.getSimpleName();
    @BindView(R.id.recycler_view_reviews)
    RecyclerView recyclerView;
    private Deque<ReviewModel> reviewModelList = new LinkedList<>();
    private ReviewAdapter adapter;

    @BindView(R.id.relative_layout_reviews_content)
    RelativeLayout rlContent;
    @BindView(R.id.progress_bar_reviews)
    ProgressBar progressBar;
    @BindView(R.id.progress_bar_under_recycler_view)
    ProgressBar pbUnderRecyclerView;
    @BindView(R.id.text_detailed_reviews_count)
    TextView tvReviewsCount;
    @BindView(R.id.button_send_user_review)
    Button btnSendReview;
    @BindView(R.id.edit_text_user_review)
    EditText etReview;
    @BindView(R.id.relative_layout_more_responses)
    RelativeLayout rlMoreResponses;
    @BindView(R.id.progress_bar_above_reviews)
    ProgressBar pbAboveReviews;
    @BindView(R.id.swipe_refresh_layout_reviews)
    SwipeRefreshLayout swipeRefreshLayout;

    private SharedPreferences sharedPreferences = null;
    private String url = "";
    private String venueId = "";
    private int totalReviewsCount = 0;
    private int currentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);
        setToolbar();

        swipeRefreshLayout.setOnRefreshListener(this);

        Intent intent = getIntent();
        url = Const.URL_GET_REVIEWS + intent.getStringExtra(Const.venueId);
        venueId = intent.getStringExtra(Const.venueId);
        sharedPreferences = getSharedPreferences(Preferences.shPref, 0);

        prepareRecyclerView();
        loadUsersReviews(String.valueOf(currentPage));
    }

    private void prepareRecyclerView(){
        adapter = new ReviewAdapter(ReviewsActivity.this, reviewModelList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ReviewsActivity.this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void loadUsersReviews(String currentPage){
        AndroidNetworking.get(url)
                .addHeaders("accessToken", sharedPreferences.getString(Preferences.userAccessToken, null))
                .addQueryParameter("page", currentPage)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("statusCode").equals("200")){
                                JSONObject result = response.getJSONObject("result");
                                JSONObject details = result.getJSONObject("details");
                                tvReviewsCount.setText(details.getString("count"));

                                JSONArray reviews = result.getJSONArray("reviews");
                                for(int i = 0; i < reviews.length(); i++){
                                    JSONObject reviewsObject = reviews.getJSONObject(i);
                                    ReviewModel reviewModel = new ReviewModel();
                                    reviewModel.setUserName(reviewsObject.getString("name"));
                                    reviewModel.setReviewTime(reviewsObject.getString("date"));
                                    reviewModel.setUserReview(reviewsObject.getString("text"));

                                    reviewModelList.addLast(reviewModel);
                                }

                                totalReviewsCount = Integer.parseInt(details.getString("count"));

                                if(reviewModelList.size() < totalReviewsCount)
                                    rlMoreResponses.setVisibility(View.VISIBLE);
                                else
                                    rlMoreResponses.setVisibility(View.GONE);


                                adapter.notifyDataSetChanged();
                                pbUnderRecyclerView.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);
                                progressBar.setVisibility(View.GONE);
                                rlContent.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject response = new JSONObject(anError.getErrorBody());
                            if(response.getString("statusCode").equals("401")){

                                Toast.makeText(ReviewsActivity.this, getString(R.string.session_error), Toast.LENGTH_LONG);

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Preferences.userAccessToken, null);
                                editor.putBoolean(Preferences.logged, false);
                                editor.commit();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(ReviewsActivity.this, CheckUserStatus.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    }
                                }, 3500);
                            } else if(response.getString("statusCode").equals("500")){
                                Toast.makeText(ReviewsActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                                rlContent.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);
                                pbUnderRecyclerView.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);

                            } else if(response.getString("statusCode").equals("408")){
                                Toast.makeText(ReviewsActivity.this, getString(R.string.timeout_error), Toast.LENGTH_LONG).show();
                                rlContent.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);
                                pbUnderRecyclerView.setVisibility(View.GONE);
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(Const.reviewsCount, tvReviewsCount.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.button_send_user_review)
    public void btnSendReview(){
        if(!etReview.getText().toString().equals("")){
            pbAboveReviews.setVisibility(View.VISIBLE);

            final SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);

            Map<String, String> params = new HashMap<>();
            params.put("venue_id", venueId);
            params.put("text", etReview.getText().toString());

            AndroidNetworking.post(Const.URL_ADD_REVIEW)
                    .addHeaders("accessToken", sharedPreferences.getString(Preferences.userAccessToken, null))
                    .addBodyParameter(params)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if(response.getString("statusCode").equals("201")){
                                    etReview.setText("");

                                    JSONObject result = response.getJSONObject("result");
                                    ReviewModel reviewModel = new ReviewModel();
                                    reviewModel.setUserName(result.getString("name"));
                                    reviewModel.setUserReview(result.getString("text"));
                                    reviewModel.setReviewTime(result.getString("date"));

                                    if(reviewModelList.size() == 10)
                                        reviewModelList.removeLast();

                                    reviewModelList.addFirst(reviewModel);
                                    totalReviewsCount = totalReviewsCount + 1;
                                    tvReviewsCount.setText(String.valueOf(totalReviewsCount));

                                    if(reviewModelList.size() < totalReviewsCount)
                                        rlMoreResponses.setVisibility(View.VISIBLE);
                                    else
                                        rlMoreResponses.setVisibility(View.GONE);

                                    pbAboveReviews.setVisibility(View.GONE);
                                    adapter.notifyDataSetChanged();
                                }
                            } catch (JSONException e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            try {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                if(response.getString("statusCode").equals("401")){

                                    Toast.makeText(ReviewsActivity.this, getString(R.string.session_error), Toast.LENGTH_LONG).show();

                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString(Preferences.userAccessToken, null);
                                    editor.putBoolean(Preferences.logged, false);
                                    editor.commit();

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            startActivity(new Intent(ReviewsActivity.this, CheckUserStatus.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                        }
                                    }, 3500);
                                } else if(response.getString("statusCode").equals("500")){
                                    Toast.makeText(ReviewsActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                                    rlContent.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                    pbUnderRecyclerView.setVisibility(View.GONE);
                                    swipeRefreshLayout.setRefreshing(false);

                                } else if(response.getString("statusCode").equals("408")){
                                    Toast.makeText(ReviewsActivity.this, getString(R.string.timeout_error), Toast.LENGTH_LONG).show();
                                    rlContent.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                    swipeRefreshLayout.setRefreshing(false);
                                    pbUnderRecyclerView.setVisibility(View.GONE);
                                }
                            } catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    });

        } else {
            Toast.makeText(ReviewsActivity.this, getString(R.string.fill_review), Toast.LENGTH_SHORT).show();
        }
    }

    private void setDefaultValues(){
        currentPage = 1;
        totalReviewsCount = 0;
        reviewModelList.clear();
    }

    @OnClick(R.id.relative_layout_more_responses)
    public void rlMoreResponses(){
        currentPage = currentPage + 1;
        rlMoreResponses.setVisibility(View.GONE);
        pbUnderRecyclerView.setVisibility(View.VISIBLE);
        loadUsersReviews(String.valueOf(currentPage));
    }

    private void setToolbar(){
        Intent intent = getIntent();
        getTvToolbarTitle().setText(intent.getStringExtra(Const.venueName));
        getToolbar().setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.arrow));
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getSearchMenuItem().setVisible(false);
        getFilterMenuItem().setVisible(false);
        getOkMenuItem().setVisible(false);
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public void onRefresh() {
        setDefaultValues();
        loadUsersReviews(String.valueOf(currentPage));
    }
}
