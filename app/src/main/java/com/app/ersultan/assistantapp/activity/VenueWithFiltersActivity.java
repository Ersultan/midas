package com.app.ersultan.assistantapp.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.ersultan.assistantapp.CheckUserStatus;
import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.adapter.MoreVenueAdapter;
import com.app.ersultan.assistantapp.helper.Const;
import com.app.ersultan.assistantapp.helper.GridSpacingItemDecoration;
import com.app.ersultan.assistantapp.helper.Preferences;
import com.app.ersultan.assistantapp.helper.RecyclerTouchListener;
import com.app.ersultan.assistantapp.model.DistrictModel;
import com.app.ersultan.assistantapp.model.FiltersModel;
import com.app.ersultan.assistantapp.model.InformationModel;
import com.app.ersultan.assistantapp.model.PricesModel;
import com.app.ersultan.assistantapp.model.SingleItemModel;
import com.app.ersultan.assistantapp.model.SortingModel;
import com.app.ersultan.assistantapp.model.SubPricesModel;
import com.app.ersultan.assistantapp.model.ValuesOfFiltersModel;
import com.app.ersultan.assistantapp.model.WorkTimeModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import dmax.dialog.SpotsDialog;

public class VenueWithFiltersActivity extends BaseDrawerActivity implements SwipeRefreshLayout.OnRefreshListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    public static final String TAG = VenueWithFiltersActivity.class.getSimpleName();
    private MoreVenueAdapter adapter;
    private List<SingleItemModel> venuesList = new ArrayList<>();
    @BindView(R.id.recycler_view_venue_with_filters)
    RecyclerView recyclerView;

    @BindView(R.id.progress_bar_venue_with_filters)
    ProgressBar progressBar;
    @BindView(R.id.relative_layout_venue_with_filters_content)
    RelativeLayout rlContent;
    @BindView(R.id.button_show_filter_result)
    Button btnShowFilterResults;
    @BindView(R.id.llFilters)
    LinearLayout llFilters;

    @BindView(R.id.llDistrict)
    LinearLayout llDistrict;
    @BindView(R.id.text_chosen_district)
    TextView tvDistrict;

    @BindView(R.id.llWorkTime)
    LinearLayout llWorkTime;
    @BindView(R.id.text_chosen_work_time)
    TextView tvWorkTime;
    @BindView(R.id.llFiltersOther)
    LinearLayout llFiltersOther;
    @BindView(R.id.text_request_result)
    TextView tvNotFound;
    @BindView(R.id.progress_bar_loading_venues)
    ProgressBar pbLoadingVenues;

    @BindView(R.id.swipe_refresh_layout_venues_with_filters)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.nested_scroll_view_venues_with_filters)
    NestedScrollView nestedScrollView;

    private String venueCategoryId = "";
    private int countOfFilters = 0;

    public ArrayList<DistrictModel> districtModelArrayList = new ArrayList<>();
    public ArrayList<WorkTimeModel> workTimeModelArrayList = new ArrayList<>();

    public ArrayList<FiltersModel> filtersModelArrayList = new ArrayList<>();
    public ArrayList<ValuesOfFiltersModel> valuesOfFiltersModelArrayList = new ArrayList<>();

    public ArrayList<InformationModel> informationModelArrayList = new ArrayList<>();
    public ArrayList<SortingModel> sortingModelArrayList = new ArrayList<>();

    public ArrayList<PricesModel> pricesModelArrayList = new ArrayList<>();
    public ArrayList<SubPricesModel> subPricesModelArrayList = new ArrayList<>();

    private ArrayList<LinearLayout> filterRowList = new ArrayList<>();
    private ArrayList<LinearLayout> allFiltersLayout = new ArrayList<>();
    private ArrayList<String> otherFilterTitles = new ArrayList<>();

    private String checkedRegionsId = "";
    private String checkedWorkTimeId = "";
    private String checkedOtherFiltersId = "";
    private String checkedPricesFiltersId = "";
    private String searchedText = "";

    private int REGIONS_CODE = 111;
    private int WORK_TIME_CODE = 222;
    private int OTHER_FILTER_CODE = 333;
    private int INFORMATION_CODE = 444;
    private int PRICES_CODE = 555;

    @BindView(R.id.material_search_view)
    MaterialSearchView searchView;
    private AlertDialog customProgressDialog = null;

    private int currentPage = 1;
    private int currentSearchPage = 1;
    private boolean allDataLoaded = false;
    private boolean allSearchDataLoaded = false;
    private String typedSearchText = "";

    private Map<String, String> filterParams = new HashMap<String, String>(){{
            put("page", "1");
            put("perPage", "10");
            put("information_filter", "");
            put("filter_value_ids", "");
            put("region_ids", "");
            put("work_time_id", "");
            put("search_text", "");
            put("sort_id", "");
            put("lat", "");
            put("lng", "");
            put("price_filter", "");
        }};

    private Map<String, String> searchParams = new HashMap<String, String>(){{
            put("page", "1");
            put("perPage", "10");
            put("search_text", "");
        }};

    private CharSequence[] filterSequence = null;

    private double currentLocationLatitude = 0.0;
    private double currentLocationLongitude = 0.0;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 199;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_with_filters);
        setToolbar();

        customProgressDialog = new SpotsDialog(this, R.style.CustomProgressDialog);
        customProgressDialog.setCancelable(false);
        swipeRefreshLayout.setOnRefreshListener(this);
        Intent intent = getIntent();
        venueCategoryId = intent.getStringExtra(Const.venuesCategoryId);
        searchViewListener();
        prepareRecyclerView();

        progressBar.setVisibility(View.VISIBLE);
        rlContent.setVisibility(View.GONE);
        Map<String, String> params = new HashMap<>();
        loadVenues(params);
    }

    @OnClick(R.id.llDistrict)
    public void llDistrict(){
        Intent intent = new Intent(VenueWithFiltersActivity.this, ChooseFilterVariantsActivity.class);
        intent.putExtra(Const.filterTitle, getString(R.string.choose_district_of_region));
        intent.putExtra(Const.filterArray, districtModelArrayList);
        intent.putExtra(Const.filterType, Const.regionType);
        intent.putExtra(Const.checkedRegionFilterItems, checkedRegionsId);
        startActivityForResult(intent, REGIONS_CODE);
    }

    @OnClick(R.id.llWorkTime)
    public void llWorkTime(){
        Intent intent = new Intent(VenueWithFiltersActivity.this, ChooseFilterVariantsActivity.class);
        intent.putExtra(Const.filterTitle, getString(R.string.work_time));
        intent.putExtra(Const.filterArray, workTimeModelArrayList);
        intent.putExtra(Const.filterType, Const.workTimeType);
        intent.putExtra(Const.checkedWorkTimeFilterItems, checkedWorkTimeId);
        startActivityForResult(intent, WORK_TIME_CODE);
    }

    private void prepareRecyclerView(){
        adapter = new MoreVenueAdapter(this, venuesList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(5), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(VenueWithFiltersActivity.this, DetailedItemActivity.class);
                intent.putExtra(Const.venueId, venuesList.get(position).getId());
                intent.putExtra(Const.venueType, venuesList.get(position).getType());
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) { }
        }));

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if(!allDataLoaded){
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Log.i(TAG, "SEARCH VIEW IS SHOWN: " + String.valueOf(searchView.isSearchOpen()));
                                if(searchView.isSearchOpen()){
                                    if(!allSearchDataLoaded){
                                        currentSearchPage = currentSearchPage + 1;
                                        searchParams.put("page", String.valueOf(currentSearchPage));
                                        loadVenues(searchParams);
                                    }
                                } else {
                                    if(!allDataLoaded){
                                        currentPage = currentPage + 1;
                                        filterParams.put("page", String.valueOf(currentPage));
                                        loadVenues(filterParams);
                                    }
                                }
                            }
                        }, 50);
                    }
                }
            }
        });
    }

    private void loadVenues(Map<String, String> params){
        Intent intent = getIntent();
        String url = Const.URL_GET_VENUES_BY_NAVIGATION_DRAWER_CATEGORY_ID + intent.getStringExtra(Const.venuesCategoryId);
        final SharedPreferences sharedPreferences = getSharedPreferences(Preferences.shPref, 0);

        AndroidNetworking.get(url)
                .addHeaders("accessToken", sharedPreferences.getString(Preferences.userAccessToken, null))
                .addQueryParameter(params)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("statusCode").equals("200")){
                                JSONObject result = response.getJSONObject("result");
                                JSONArray venues = result.getJSONArray("venues");
                                for(int i = 0; i < venues.length(); i++){
                                    JSONObject venuesItem = venues.getJSONObject(i);
                                    SingleItemModel singleItemModel = new SingleItemModel();
                                    singleItemModel.setId(venuesItem.getString("id"));
                                    singleItemModel.setImageURL(Const.DOMAIN_URL + venuesItem.getString("image"));
                                    singleItemModel.setReviews(venuesItem.getString("reviews_count"));
                                    singleItemModel.setRating(venuesItem.getString("rating"));
                                    singleItemModel.setType(venuesItem.getString("category_title"));
                                    singleItemModel.setName(venuesItem.getString("title"));
                                    singleItemModel.setDiscount(venuesItem.getString("discount"));
                                    venuesList.add(singleItemModel);
                                }

                                JSONObject details = result.getJSONObject("details");
                                int totalCount = details.getInt("count");
                                if(venues.length() == 0 || venuesList.size() == totalCount){
                                    allDataLoaded = true;
                                    allSearchDataLoaded = true;
                                    pbLoadingVenues.setVisibility(View.GONE);
                                } else {
                                    allDataLoaded = false;
                                    allSearchDataLoaded = false;
                                    pbLoadingVenues.setVisibility(View.VISIBLE);
                                }

                                countOfFilters = 0;
                                if(result.has("prices")){
                                    JSONArray prices = result.getJSONArray("prices");
                                    if(prices.length() > 0){
                                        countOfFilters = 1;
                                    }
                                }

                                countOfFilters += result.getJSONArray("filters").length() + result.getJSONArray("information").length();

                                Log.i(TAG, "FILTERS LENGTH: " + result.getJSONArray("filters").length());
                                Log.i(TAG, "INFORMATION LENGTH: " + result.getJSONArray("information").length());
                                Log.i(TAG, "COUNT OF FILTERS: " + countOfFilters);

                                // fill sorting array
                                sortingModelArrayList.clear();
                                JSONArray sortingArray = result.getJSONArray("sorting");
                                filterSequence = new CharSequence[sortingArray.length()];
                                for(int i = 0; i < sortingArray.length(); i++){
                                    JSONObject sortingObject = sortingArray.getJSONObject(i);
                                    SortingModel sortingModel = new SortingModel();
                                    sortingModel.setId(sortingObject.getString("id"));
                                    sortingModel.setValue(sortingObject.getString("value"));
                                    sortingModel.setChecked(sortingObject.getString("checked"));
                                    sortingModelArrayList.add(sortingModel);
                                    filterSequence[i] = sortingObject.getString("value");
                                }

                                // fill regions
                                districtModelArrayList.clear();
                                checkedRegionsId = "";
                                String regionValues = "";
                                JSONArray regions = result.getJSONArray("regions");
                                for(int i = 0; i < regions.length(); i++){
                                    JSONObject district = regions.getJSONObject(i);
                                    DistrictModel districtModel = new DistrictModel();
                                    districtModel.setDistrictId(district.getString("id"));
                                    districtModel.setDistrictTitle(district.getString("title"));
                                    districtModel.setChecked(district.getString("checked"));
                                    districtModelArrayList.add(districtModel);

                                    if(district.getString("checked").equals("1")){
                                        checkedRegionsId += district.getString("id") + ",";
                                        regionValues += district.getString("title");
                                    }
                                }

                                if(!regionValues.equals("")){
                                    tvDistrict.setText(regionValues);
                                    checkedRegionsId = checkedRegionsId.substring(0, checkedRegionsId.length() - 1);
                                } else {
                                    tvDistrict.setText(getString(R.string.choose_district_of_region));
                                }

                                // fill work times
                                workTimeModelArrayList.clear();
                                checkedWorkTimeId = "";
                                String timeValue = "";
                                JSONArray workTimes = result.getJSONArray("work_times");
                                for(int i = 0; i < workTimes.length(); i++){
                                    JSONObject workTimesObject = workTimes.getJSONObject(i);
                                    WorkTimeModel workTimeModel = new WorkTimeModel();
                                    workTimeModel.setTimeId(workTimesObject.getString("id"));
                                    workTimeModel.setTime(workTimesObject.getString("value"));
                                    workTimeModel.setChecked(workTimesObject.getString("checked"));
                                    workTimeModelArrayList.add(workTimeModel);

                                    if(workTimesObject.getString("checked").equals("1")){
                                        checkedWorkTimeId += workTimesObject.getString("id") + ",";
                                        timeValue += workTimesObject.getString("value");
                                    }
                                }

                                if(!timeValue.equals("")){
                                    tvWorkTime.setText(timeValue);
                                    checkedWorkTimeId = checkedWorkTimeId.substring(0, checkedWorkTimeId.length() - 1);
                                } else {
                                    tvWorkTime.setText(getString(R.string.choose_work_time));
                                }

                                filtersModelArrayList.clear();
                                otherFilterTitles.clear();
                                checkedOtherFiltersId = "";
                                // fill filters
                                JSONArray filters = result.getJSONArray("filters");
                                for(int i = 0; i < filters.length(); i++){
                                    JSONObject filtersObject = filters.getJSONObject(i);
                                    valuesOfFiltersModelArrayList = new ArrayList<>();

                                    String valueString = "";
                                    JSONArray values = filtersObject.getJSONArray("values");
                                    for(int j = 0; j < values.length(); j++){
                                        JSONObject valuesObject = values.getJSONObject(j);
                                        ValuesOfFiltersModel valuesOfFiltersModel = new ValuesOfFiltersModel();
                                        valuesOfFiltersModel.setValueId(valuesObject.getString("filter_value_id"));
                                        valuesOfFiltersModel.setValueTitle(valuesObject.getString("value"));
                                        valuesOfFiltersModel.setChecked(valuesObject.getString("checked"));
                                        valuesOfFiltersModelArrayList.add(valuesOfFiltersModel);

                                        Log.i(TAG, "FILTER TITLE: " + valuesObject.getString("value"));

                                        if(valuesObject.getString("checked").equals("1")){
                                            valueString += valuesObject.getString("value") + ", ";
                                            checkedOtherFiltersId += valuesObject.getString("filter_value_id") + ",";
                                        }
                                    }

                                    if(!valueString.equals("")){
                                        valueString = valueString.substring(0, valueString.length() - 2);
                                        checkedOtherFiltersId = checkedOtherFiltersId.substring(0, checkedOtherFiltersId.length() - 1);
                                    }

                                    FiltersModel filtersModel = new FiltersModel();
                                    filtersModel.setFiltersId(filtersObject.getString("filter_id"));
                                    filtersModel.setFiltersTitle(filtersObject.getString("filter_title"));
                                    filtersModel.setIsLong(filtersObject.getString("is_long"));
                                    filtersModel.setValuesOfFiltersModelArrayList(valuesOfFiltersModelArrayList);
                                    filtersModel.setValuesString(valueString);
                                    otherFilterTitles.add(filtersObject.getString("filter_title"));
                                    filtersModelArrayList.add(filtersModel);
                                }

                                // fill information
                                informationModelArrayList.clear();
                                JSONArray information = result.getJSONArray("information");
                                for(int i = 0; i < information.length(); i++){
                                    JSONObject informationObj = information.getJSONObject(i);
                                    InformationModel informationModel = new InformationModel();
                                    informationModel.setInformationId(informationObj.getString("information_id"));
                                    informationModel.setInformationTitle(informationObj.getString("information_title"));
                                    informationModel.setMinValue(informationObj.getString("min_value"));
                                    informationModel.setMaxValue(informationObj.getString("max_value"));
                                    informationModel.setDescription(informationObj.getString("description"));
                                    if(informationObj.has("value")){
                                        informationModel.setValue(informationObj.getString("value"));
                                    } else {
                                        informationModel.setValue("");
                                    }
                                    informationModel.setEndString(informationObj.getString("end_string"));
                                    otherFilterTitles.add(informationObj.getString("information_title"));
                                    informationModelArrayList.add(informationModel);
                                }

                                // fill prices
                                pricesModelArrayList.clear();
                                checkedPricesFiltersId = "";
                                JSONArray prices = result.getJSONArray("prices");
                                for(int i = 0; i < prices.length(); i++){
                                    JSONObject priceObject = prices.getJSONObject(i);
                                    subPricesModelArrayList = new ArrayList<>();

                                    //String priceString = "";
                                    JSONArray subPrices = priceObject.getJSONArray("sub_prices");
                                    for(int j = 0; j < subPrices.length(); j++){
                                        JSONObject subPriceObject = subPrices.getJSONObject(j);
                                        SubPricesModel subPricesModel = new SubPricesModel();
                                        subPricesModel.setId(subPriceObject.getString("price_id"));
                                        subPricesModel.setTitle(subPriceObject.getString("price_title"));
                                        subPricesModel.setValue(subPriceObject.getString("value"));

                                        if(!subPriceObject.getString("value").equals("") || subPriceObject.getString("value").equals("-1")){
                                            subPricesModel.setChecked("1");
                                        } else {
                                            subPricesModel.setChecked("-1");
                                        }

                                        subPricesModelArrayList.add(subPricesModel);
                                    }

                                    PricesModel pricesModel = new PricesModel();
                                    pricesModel.setId(priceObject.getString("id"));
                                    pricesModel.setTitle(priceObject.getString("title"));
                                    pricesModel.setSubPricesModels(subPricesModelArrayList);
                                    pricesModelArrayList.add(pricesModel);
                                }

                                if(pricesModelArrayList.size() > 0){
                                    otherFilterTitles.add(getString(R.string.services));
                                }

                                LinearLayout.LayoutParams lpRoot = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                lpRoot.setMargins(0, 10, 0, 0);

                                llFiltersOther.removeAllViews();
                                filterRowList.clear();
                                allFiltersLayout.clear();

                                int isLongCount = 0;
                                for(int i = 0; i < filtersModelArrayList.size(); i++){
                                    if(filtersModelArrayList.get(i).getIsLong().equals("1")){
                                        isLongCount = isLongCount + 1;

                                        LinearLayout llRoot = new LinearLayout(VenueWithFiltersActivity.this);
                                        llRoot.setLayoutParams(lpRoot);
                                        llRoot.setOrientation(LinearLayout.HORIZONTAL);
                                        llFiltersOther.addView(llRoot);

                                        LinearLayout.LayoutParams lpFilterZero = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                                            lpFilterZero.setMargins(10, 10, 10, 0);
                                        } else {
                                            lpFilterZero.setMargins(7, 5, 7, 0);
                                        }


                                        LinearLayout llFilter = new LinearLayout(VenueWithFiltersActivity.this);
                                        llFilter.setOrientation(LinearLayout.HORIZONTAL);
                                        llFilter.setBackground(ContextCompat.getDrawable(VenueWithFiltersActivity.this, R.drawable.features_form));
                                        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                                            llFilter.setPadding(20, 20, 20, 20);
                                        } else {
                                            llFilter.setPadding(15, 15, 15, 15);
                                        }
                                        llFilter.setLayoutParams(lpFilterZero);

                                        llFiltersOther.addView(llFilter);
                                        allFiltersLayout.add(llFilter);


                                        TextView tvFilterTitle = new TextView(VenueWithFiltersActivity.this);
                                        tvFilterTitle.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
                                        if(filtersModelArrayList.get(i).getValuesString().equals("")){
                                            tvFilterTitle.setText(filtersModelArrayList.get(i).getFiltersTitle());
                                        } else {
                                            tvFilterTitle.setText(filtersModelArrayList.get(i).getValuesString());
                                        }
                                        tvFilterTitle.setEllipsize(TextUtils.TruncateAt.END);
                                        tvFilterTitle.setMaxLines(1);
                                        tvFilterTitle.setTextColor(ContextCompat.getColor(VenueWithFiltersActivity.this, R.color.colorPrimaryDark));
                                        llFilter.addView(tvFilterTitle);

                                        LinearLayout.LayoutParams lpArrow = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                        lpArrow.gravity = Gravity.CENTER_VERTICAL;

                                        ImageView ivArrow = new ImageView(VenueWithFiltersActivity.this);
                                        ivArrow.setLayoutParams(lpArrow);
                                        ivArrow.setBackground(ContextCompat.getDrawable(VenueWithFiltersActivity.this, R.drawable.dark_dropdown));
                                        llFilter.addView(ivArrow);
                                    } else {
                                        break;
                                    }
                                }

                                Log.i(TAG, "IS LONG COUNT: " + isLongCount);

                                if(((countOfFilters - isLongCount) % 2) == 0){
                                    Log.i(TAG, "EVEN");
                                    for(int i = isLongCount; i < countOfFilters; i++){
                                        LinearLayout llRoot = new LinearLayout(VenueWithFiltersActivity.this);
                                        llRoot.setLayoutParams(lpRoot);
                                        llRoot.setOrientation(LinearLayout.HORIZONTAL);
                                        llFiltersOther.addView(llRoot);

                                        LinearLayout.LayoutParams lpFilterNotZero = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
                                        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                                            lpFilterNotZero.setMargins(10, 0, 10, 0);
                                        } else {
                                            lpFilterNotZero.setMargins(7, 0, 7, 0);
                                        }

                                        LinearLayout llFilter = new LinearLayout(VenueWithFiltersActivity.this);
                                        llFilter.setOrientation(LinearLayout.HORIZONTAL);
                                        llFilter.setBackground(ContextCompat.getDrawable(VenueWithFiltersActivity.this, R.drawable.features_form));
                                        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                                            llFilter.setPadding(20, 20, 20, 20);
                                        } else {
                                            llFilter.setPadding(15, 15, 15, 15);
                                        }
                                        llFilter.setLayoutParams(lpFilterNotZero);
                                        filterRowList.add(llFilter);

                                        if(filterRowList.size() == 2){
                                            for(int j = 0; j < filterRowList.size(); j++){
                                                llRoot.addView(filterRowList.get(j));
                                                allFiltersLayout.add(filterRowList.get(j));
                                            }
                                            filterRowList.clear();
                                        }

                                        TextView tvFilterTitle = new TextView(VenueWithFiltersActivity.this);
                                        tvFilterTitle.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
                                        if(i < filtersModelArrayList.size()){
                                            if(filtersModelArrayList.get(i).getValuesString().equals("")){
                                                tvFilterTitle.setText(filtersModelArrayList.get(i).getFiltersTitle());
                                            } else {
                                                tvFilterTitle.setText(filtersModelArrayList.get(i).getValuesString());
                                            }
                                        } else if(i >= filtersModelArrayList.size() && i < filtersModelArrayList.size() + informationModelArrayList.size()){
                                            if(informationModelArrayList.get(i - filtersModelArrayList.size()).getValue().equals("")){
                                                tvFilterTitle.setText(informationModelArrayList.get(i - filtersModelArrayList.size()).getInformationTitle());
                                            } else {
                                                if(informationModelArrayList.get(i - filtersModelArrayList.size()).getEndString().equals("")){
                                                    tvFilterTitle.setText(informationModelArrayList.get(i - filtersModelArrayList.size()).getValue());
                                                } else {
                                                    tvFilterTitle.setText(informationModelArrayList.get(i - filtersModelArrayList.size()).getValue() + " " + informationModelArrayList.get(i - filtersModelArrayList.size()).getEndString());
                                                }
                                            }
                                        } else if(!pricesModelArrayList.isEmpty()){
                                            tvFilterTitle.setText(getString(R.string.services));
                                        }
                                        tvFilterTitle.setEllipsize(TextUtils.TruncateAt.END);
                                        tvFilterTitle.setMaxLines(1);
                                        tvFilterTitle.setTextColor(ContextCompat.getColor(VenueWithFiltersActivity.this, R.color.colorPrimaryDark));
                                        //tvFilterTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                                        llFilter.addView(tvFilterTitle);

                                        LinearLayout.LayoutParams lpArrow = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                        lpArrow.gravity = Gravity.CENTER_VERTICAL;

                                        ImageView ivArrow = new ImageView(VenueWithFiltersActivity.this);
                                        ivArrow.setLayoutParams(lpArrow);
                                        ivArrow.setBackground(ContextCompat.getDrawable(VenueWithFiltersActivity.this, R.drawable.dark_dropdown));
                                        llFilter.addView(ivArrow);
                                    }

                                } else {
                                    Log.i(TAG, "ODD");
                                    for(int i = isLongCount; i < countOfFilters; i++){
                                        LinearLayout llRoot = new LinearLayout(VenueWithFiltersActivity.this);
                                        llRoot.setLayoutParams(lpRoot);
                                        llRoot.setOrientation(LinearLayout.HORIZONTAL);
                                        llFiltersOther.addView(llRoot);

                                        LinearLayout.LayoutParams lpFilterZero = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                                            lpFilterZero.setMargins(10, 10, 10, 0);
                                        } else {
                                            lpFilterZero.setMargins(7, 5, 7, 0);
                                        }

                                        LinearLayout.LayoutParams lpFilterNotZero = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
                                        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                                            lpFilterNotZero.setMargins(10, 0, 10, 0);
                                        } else {
                                            lpFilterNotZero.setMargins(7, 0, 7, 0);
                                        }

                                        LinearLayout llFilter = new LinearLayout(VenueWithFiltersActivity.this);
                                        llFilter.setOrientation(LinearLayout.HORIZONTAL);
                                        llFilter.setBackground(ContextCompat.getDrawable(VenueWithFiltersActivity.this, R.drawable.features_form));
                                        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                                            llFilter.setPadding(20, 20, 20, 20);
                                        } else {
                                            llFilter.setPadding(15, 15, 15, 15);
                                        }

                                        if(i == isLongCount){
                                            llFilter.setLayoutParams(lpFilterZero);
                                        } else {
                                            llFilter.setLayoutParams(lpFilterNotZero);
                                            filterRowList.add(llFilter);
                                        }


                                        if(i == isLongCount) {
                                            llFiltersOther.addView(llFilter);
                                            allFiltersLayout.add(llFilter);
                                        } else {
                                            if(filterRowList.size() == 2){
                                                for(int j = 0; j < filterRowList.size(); j++){
                                                    llRoot.addView(filterRowList.get(j));
                                                    allFiltersLayout.add(filterRowList.get(j));
                                                }
                                                filterRowList.clear();
                                            }
                                        }
                                        TextView tvFilterTitle = new TextView(VenueWithFiltersActivity.this);
                                        tvFilterTitle.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
                                        if(i < filtersModelArrayList.size()){
                                            if(filtersModelArrayList.get(i).getValuesString().equals("")){
                                                tvFilterTitle.setText(filtersModelArrayList.get(i).getFiltersTitle());
                                            } else {
                                                tvFilterTitle.setText(filtersModelArrayList.get(i).getValuesString());
                                            }
                                        } else if(i >= filtersModelArrayList.size() && i < filtersModelArrayList.size() + informationModelArrayList.size()){
                                            if(informationModelArrayList.get(i - filtersModelArrayList.size()).getValue().equals("")){
                                                tvFilterTitle.setText(informationModelArrayList.get(i - filtersModelArrayList.size()).getInformationTitle());
                                            } else {
                                                if(informationModelArrayList.get(i - filtersModelArrayList.size()).getEndString().equals("")){
                                                    tvFilterTitle.setText(informationModelArrayList.get(i - filtersModelArrayList.size()).getValue());
                                                } else {
                                                    tvFilterTitle.setText(informationModelArrayList.get(i - filtersModelArrayList.size()).getValue() + " " + informationModelArrayList.get(i - filtersModelArrayList.size()).getEndString());
                                                }
                                            }
                                        } else if(!pricesModelArrayList.isEmpty()){
                                            tvFilterTitle.setText(getString(R.string.services));
                                        }
                                        tvFilterTitle.setEllipsize(TextUtils.TruncateAt.END);
                                        tvFilterTitle.setMaxLines(1);
                                        tvFilterTitle.setTextColor(ContextCompat.getColor(VenueWithFiltersActivity.this, R.color.colorPrimaryDark));
//                                        if(i < 0){
//                                            tvFilterTitle.setGravity(Gravity.CENTER_HORIZONTAL);
//                                        }
                                        llFilter.addView(tvFilterTitle);

                                        LinearLayout.LayoutParams lpArrow = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                        lpArrow.gravity = Gravity.CENTER_VERTICAL;

                                        ImageView ivArrow = new ImageView(VenueWithFiltersActivity.this);
                                        ivArrow.setLayoutParams(lpArrow);
                                        ivArrow.setBackground(ContextCompat.getDrawable(VenueWithFiltersActivity.this, R.drawable.dark_dropdown));
                                        llFilter.addView(ivArrow);
                                    }

                                }

                                filterLayoutClickListener();
                                Log.i(TAG, "ALL FILTER LAYOUT SIZE: " + allFiltersLayout.size());

                                if(venuesList.size() == 0){
                                    tvNotFound.setVisibility(View.VISIBLE);
                                } else {
                                    tvNotFound.setVisibility(View.GONE);
                                }

                                swipeRefreshLayout.setRefreshing(false);
                                adapter.notifyDataSetChanged();
                                progressBar.setVisibility(View.GONE);
                                rlContent.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.VISIBLE);
                                customProgressDialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject response = new JSONObject(anError.getErrorBody());
                            if(response.getString("statusCode").equals("401")){

                                Toast.makeText(VenueWithFiltersActivity.this, getString(R.string.session_error), Toast.LENGTH_LONG).show();

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Preferences.userAccessToken, null);
                                editor.putBoolean(Preferences.logged, false);
                                editor.commit();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(VenueWithFiltersActivity.this, CheckUserStatus.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    }
                                }, 3500);
                            } else if(response.getString("statusCode").equals("500")){
                                Toast.makeText(VenueWithFiltersActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                                rlContent.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);
                                customProgressDialog.dismiss();
                                swipeRefreshLayout.setRefreshing(false);

                            } else if(response.getString("statusCode").equals("408")){
                                Toast.makeText(VenueWithFiltersActivity.this, getString(R.string.timeout_error), Toast.LENGTH_LONG).show();
                                rlContent.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);
                                customProgressDialog.dismiss();
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void filterLayoutClickListener(){
        for(int i = 0; i < allFiltersLayout.size(); i++){
            final int finalI = i;
            allFiltersLayout.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(finalI < filtersModelArrayList.size()){
                        Intent intent = new Intent(VenueWithFiltersActivity.this, ChooseFilterVariantsActivity.class);
                        intent.putExtra(Const.filterTitle, otherFilterTitles.get(finalI).toString());
                        intent.putExtra(Const.filterArray, filtersModelArrayList.get(finalI).getValuesOfFiltersModelArrayList());
                        intent.putExtra(Const.filterType, Const.otherFilterType);
                        intent.putExtra(Const.layoutPosition, finalI);
                        intent.putExtra(Const.otherFilterPosition, finalI);
                        intent.putExtra(Const.checkedOtherFilterItems, checkedOtherFiltersId);
                        startActivityForResult(intent, OTHER_FILTER_CODE);

                    } else if(finalI >= filtersModelArrayList.size() && finalI < filtersModelArrayList.size() + informationModelArrayList.size()){
                        Intent intent = new Intent(VenueWithFiltersActivity.this, InformationFilterActivity.class);
                        intent.putExtra(Const.filterTitle, otherFilterTitles.get(finalI).toString());
                        intent.putExtra(Const.filterModel, informationModelArrayList.get(finalI - filtersModelArrayList.size()));
                        intent.putExtra(Const.informationPosition, finalI - filtersModelArrayList.size());
                        intent.putExtra(Const.layoutPosition, finalI);
                        startActivityForResult(intent, INFORMATION_CODE);

                    } else if(!pricesModelArrayList.isEmpty()){
                        Intent intent = new Intent(VenueWithFiltersActivity.this, PriceFilterActivity.class);
                        intent.putExtra(Const.filterArray, pricesModelArrayList);
                        startActivityForResult(intent, PRICES_CODE);
                    }

                }
            });
        }
    }

    @OnClick(R.id.button_show_filter_result)
    public void btnShowFilterResult(){
        setDefaultValues();

        String informationJson = "";
        String informationIds = "";
        String informationValues = "";
        String filtersIds = "";

        // convert information to json
        for(int i = 0; i < informationModelArrayList.size(); i++){
            Log.i(TAG, "INFO VALUE: " + informationModelArrayList.get(i).getValue());
            if(!informationModelArrayList.get(i).getValue().equals("") && !informationModelArrayList.get(i).getValue().equals(informationModelArrayList.get(i).getMinValue()) && !informationModelArrayList.get(i).getValue().equals(informationModelArrayList.get(i).getMinValue() + " " + informationModelArrayList.get(i).getEndString())){
                informationIds += informationModelArrayList.get(i).getInformationId() + ",";
                informationValues += informationModelArrayList.get(i).getValue().replaceAll(informationModelArrayList.get(i).getEndString(), "").replaceAll(" ", "") + ",";
            }
        }

        if(!informationIds.equals("")){
            informationIds = informationIds.substring(0, informationIds.length() - 1);
            informationValues = informationValues.substring(0, informationValues.length() - 1);
            informationJson = "{\"id\":[" + informationIds + "],\"value\":[" + informationValues + "]}";
        }

        // convert filter items to array
        if(!checkedOtherFiltersId.equals("") && !checkedOtherFiltersId.equals(",")){
            filtersIds = checkedOtherFiltersId.substring(0, checkedOtherFiltersId.length() - 1);
        }


        String id = "";
        String values = "";

        for(int i = 0; i < pricesModelArrayList.size(); i++){
            for(int j = 0; j < pricesModelArrayList.get(i).getSubPricesModels().size(); j++){
                if(pricesModelArrayList.get(i).getSubPricesModels().get(j).getChecked().equals("1")){
                    id += pricesModelArrayList.get(i).getSubPricesModels().get(j).getId() + ",";
                    if(pricesModelArrayList.get(i).getSubPricesModels().get(j).getValue().equals(getString(R.string.price_with_symbol))){
                        values += "-1,";
                    } else {
                        values += pricesModelArrayList.get(i).getSubPricesModels().get(j).getValue() + ",";
                    }
                }
            }
        }

        if(!id.isEmpty()){
            values = values.replaceAll(" тг.", "");

            id = id.substring(0, id.length() - 1);
            values = values.substring(0, values.length() - 1);
            checkedPricesFiltersId = "{\"id\":[" + id + "],\"value\":[" + values + "]}";
        }

        Log.i(TAG, "CHECKED PRICES:" + checkedPricesFiltersId);

        filterParams.put("information_filter", informationJson);
        filterParams.put("filter_value_ids", "[" + filtersIds + "]");
        filterParams.put("region_ids", "[" + checkedRegionsId + "]");
        filterParams.put("work_time_id", checkedWorkTimeId);
        filterParams.put("price_filter", checkedPricesFiltersId);

        Log.i(TAG, "INFORMATION FILTER: " + informationJson);
        Log.i(TAG, "FILTER VALUES IDS: " + filtersIds);
        Log.i(TAG, "REGIONS ID: " + checkedRegionsId);
        Log.i(TAG, "WORK TIME ID: " + checkedWorkTimeId);
        Log.i(TAG, "PRICE FILTER: " + checkedPricesFiltersId);

        customProgressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadVenues(filterParams);
            }
        }, 1000);
    }

    public void searchViewListener(){
        searchView.setHint(getString(R.string.search_hint_text));
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                llFilters.setVisibility(View.GONE);
                llFiltersOther.setVisibility(View.GONE);
                btnShowFilterResults.setVisibility(View.GONE);

                setDefaultValues();
                customProgressDialog.show();
                loadVenues(searchParams);
            }

            @Override
            public void onSearchViewClosed() {
                llFilters.setVisibility(View.VISIBLE);
                llFiltersOther.setVisibility(View.VISIBLE);
                btnShowFilterResults.setVisibility(View.VISIBLE);
                setDefaultValues();
                customProgressDialog.show();
                typedSearchText = "";
                searchParams.put("search_text", typedSearchText);
                loadVenues(filterParams);
            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                setDefaultValues();
                customProgressDialog.show();
                typedSearchText = query;
                searchParams.put("search_text", typedSearchText);
                loadVenues(searchParams);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    searchView.setQuery(searchWrd, false);
                }
            }

            return;
        }

        if(requestCode == REGIONS_CODE){
            if(resultCode == RESULT_OK){
                checkedRegionsId = data.getStringExtra(Const.checkedRegionFilterItems);
                boolean found = false;
                for(int i = 0; i < districtModelArrayList.size(); i++){
                    if(districtModelArrayList.get(i).getDistrictId().equals(checkedRegionsId)){
                        tvDistrict.setText(districtModelArrayList.get(i).getDistrictTitle());
                        found = true;
                    }
                }

                if(!found){
                    tvDistrict.setText(R.string.choose_district_of_region);
                }
            }
        }

        if(requestCode == WORK_TIME_CODE){
            if(resultCode == RESULT_OK){
                checkedWorkTimeId = data.getStringExtra(Const.checkedWorkTimeFilterItems);
                boolean found = false;
                for(int i = 0; i < workTimeModelArrayList.size(); i++){
                    if(workTimeModelArrayList.get(i).getTimeId().equals(checkedWorkTimeId)){
                        tvWorkTime.setText(workTimeModelArrayList.get(i).getTime());
                        found = true;
                    }
                }

                if(!found){
                    tvWorkTime.setText(R.string.choose_work_time);
                }
            }
        }

        if(requestCode == OTHER_FILTER_CODE){
            if(resultCode == RESULT_OK){
                int otherFilterListPosition = data.getIntExtra(Const.otherFilterPosition, 0);
                int layoutPosition = data.getIntExtra(Const.layoutPosition, 0);
                checkedOtherFiltersId = data.getStringExtra(Const.checkedOtherFilterItems);

                if(!checkedOtherFiltersId.equals("") && !checkedOtherFiltersId.equals(",")){
                    String[] checkedIdArray = checkedOtherFiltersId.split(",");
                    String checkedFilterTitles = "";

                    for(int i = 0; i < filtersModelArrayList.get(otherFilterListPosition).getValuesOfFiltersModelArrayList().size(); i++){
                        for(int j = 0; j < checkedIdArray.length; j++){
                            if(filtersModelArrayList.get(otherFilterListPosition).getValuesOfFiltersModelArrayList().get(i).getValueId().equals(checkedIdArray[j])){
                                checkedFilterTitles += filtersModelArrayList.get(otherFilterListPosition).getValuesOfFiltersModelArrayList().get(i).getValueTitle() + ", ";
                            }
                        }
                    }

                    if(!checkedFilterTitles.equals("")){
                        checkedFilterTitles = checkedFilterTitles.substring(0, checkedFilterTitles.length() - 2);
                    }

                    // set checked filter titles
                    TextView textView = (TextView) allFiltersLayout.get(layoutPosition).getChildAt(0);
                    textView.setText(checkedFilterTitles);

                    if(textView.getText().equals("")){
                        textView.setText(otherFilterTitles.get(layoutPosition));
                    }

                } else {
                    TextView textView = (TextView) allFiltersLayout.get(layoutPosition).getChildAt(0);
                    textView.setText(otherFilterTitles.get(layoutPosition));
                }
            }
        }

        if(requestCode == INFORMATION_CODE){
            if(resultCode == RESULT_OK){
                int position = data.getIntExtra(Const.informationPosition, 0);
                int layoutPosition = data.getIntExtra(Const.layoutPosition, 0);
                InformationModel informationModel = (InformationModel) data.getSerializableExtra(Const.filterModel);
                informationModelArrayList.set(position, informationModel);

                Log.i(TAG, "INFORMATION VALUE: " + informationModelArrayList.get(position).getValue());
                String endString = informationModelArrayList.get(position).getEndString();
                if(!endString.equals("")){
                    endString = " " + endString;
                }

                if(!informationModelArrayList.get(position).getValue().equals("") && !informationModelArrayList.get(position).getValue().equals(informationModelArrayList.get(position).getMinValue() + endString)){
                    TextView textView = (TextView) allFiltersLayout.get(layoutPosition).getChildAt(0);
                    textView.setText(informationModelArrayList.get(position).getValue());
                } else {
                    TextView textView = (TextView) allFiltersLayout.get(layoutPosition).getChildAt(0);
                    textView.setText(otherFilterTitles.get(layoutPosition));
                }
            }
        }

        if(requestCode == PRICES_CODE){
            if(resultCode == RESULT_OK){
                if((ArrayList<PricesModel>) data.getSerializableExtra(Const.filterArray) != null){
                    pricesModelArrayList = (ArrayList<PricesModel>) data.getSerializableExtra(Const.filterArray);
                }
            }
        }
    }

    @Override
    public void onRefresh() {
        setDefaultValues();
        if(searchView.isSearchOpen()){
            loadVenues(searchParams);
        } else {
            loadVenues(filterParams);
        }
    }

    private void showFilter(){
        if(filterSequence != null){
            if(filterSequence.length > 0){
                int checkedItemPosition = 0;

                for(int i = 0; i < sortingModelArrayList.size(); i++){
                    if(sortingModelArrayList.get(i).getChecked().equals("1")){
                        checkedItemPosition = i;
                    }
                }

                new AlertDialog.Builder(this)
                        .setSingleChoiceItems(filterSequence, checkedItemPosition, null)
                        .setPositiveButton(R.string.ok_button_label, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                                final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                                if(sortingModelArrayList.get(selectedPosition).getValue().equals("Рядом") && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                                    android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(VenueWithFiltersActivity.this).create();
                                    alertDialog.setTitle(getString(R.string.disabled_gps_title));
                                    alertDialog.setMessage(getString(R.string.find_current_coordinate_text));
                                    alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE, "ДА",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                                    dialog.dismiss();
                                                }
                                            });
                                    alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEGATIVE, "НЕТ",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                    alertDialog.show();

                                } else if(sortingModelArrayList.get(selectedPosition).getValue().equals("Рядом") && currentLocationLatitude == 0.0 && currentLocationLongitude == 0.0){

                                    Toast.makeText(VenueWithFiltersActivity.this, getString(R.string.can_not_find_coordinate), Toast.LENGTH_LONG).show();

                                } else {
                                    for(int i = 0; i < sortingModelArrayList.size(); i++){
                                        if(i != selectedPosition){
                                            sortingModelArrayList.get(i).setChecked("0");
                                        } else {
                                            sortingModelArrayList.get(i).setChecked("1");
                                            filterParams.put("sort_id", sortingModelArrayList.get(i).getId());
                                            filterParams.put("lat", String.valueOf(currentLocationLatitude));
                                            filterParams.put("lng", String.valueOf(currentLocationLongitude));
                                            Log.i(TAG, "LAT: " + String.valueOf(currentLocationLatitude) + ", LNG: " + String.valueOf(currentLocationLongitude));
                                            Log.i(TAG, "ID: " + sortingModelArrayList.get(i).getId());
                                        }
                                    }

                                    customProgressDialog.show();
                                    setDefaultValues();
                                    loadVenues(filterParams);
                                }

                                Log.i(TAG, "SELECTED ITEM POSITION: " + selectedPosition);

                                dialog.dismiss();
                            }
                        })
                        .show();

            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        MenuItem okItem = menu.findItem(R.id.action_ok);
        okItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_filter:
                //Initialize Google Play Services
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        //Location Permission already granted
                        Log.i(TAG, "GRANTED!!!");
                        buildGoogleApiClient();
                    } else {
                        //Request Location Permission
                        checkLocationPermission();
                    }
                } else {
                    buildGoogleApiClient();
                }

                showFilter();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected synchronized void buildGoogleApiClient(){
        Log.i(TAG, "BUILD GOOGLE");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new android.support.v7.app.AlertDialog.Builder(this)
                        .setTitle(R.string.disabled_gps_title)
                        .setMessage(R.string.find_current_coordinate_text)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(VenueWithFiltersActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION );
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION );
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                    }

                } else {
                    showAlert();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "ON CONNECTED");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            Log.i(TAG, "LOCATION NULL");
        } else {
            //If everything went fine lets get latitude and longitude
            currentLocationLatitude = location.getLatitude();
            currentLocationLongitude = location.getLongitude();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "CONNECTION SUSPEND");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "CONNECTION FAILED");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "LOCATION CHANGED");
        currentLocationLatitude = location.getLatitude();
        currentLocationLongitude = location.getLongitude();
    }

    private void showAlert(){
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(VenueWithFiltersActivity.this).create();
            alertDialog.setTitle(getString(R.string.disabled_gps_title));
            alertDialog.setMessage(getString(R.string.find_current_coordinate_text));
            alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE, "ДА",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            dialog.dismiss();
                        }
                    });
            alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEGATIVE, "НЕТ",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
            alertDialog.show();
        }
    }

    private void setToolbar(){
        Intent intent = getIntent();
        getTvToolbarTitle().setText(intent.getStringExtra(Const.venueType));
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    private void setDefaultValues(){
        venuesList.clear();
        adapter.notifyDataSetChanged();

        currentPage = 1;
        filterParams.put("page", String.valueOf(currentPage));

        currentSearchPage = 1;
        searchParams.put("page", String.valueOf(currentSearchPage));

        allDataLoaded = false;
        allSearchDataLoaded = false;
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}