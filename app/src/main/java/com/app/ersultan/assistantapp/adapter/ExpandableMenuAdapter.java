package com.app.ersultan.assistantapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.model.MenuChildModel;
import com.app.ersultan.assistantapp.model.MenuGroupModel;
import com.app.ersultan.assistantapp.widget.ExpandableItemIndicator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ersultan on 11/9/17.
 */

public class ExpandableMenuAdapter extends AbstractExpandableItemAdapter<ExpandableMenuAdapter.MyGroupViewHolder, ExpandableMenuAdapter.MyChildViewHolder> {

    public static final String TAG = ExpandableMenuAdapter.class.getSimpleName();
    private RecyclerViewExpandableItemManager mExpandableManager;
    private Context mContext;
    private List<Pair<MenuGroupModel, List<MenuChildModel>>> data = new LinkedList<>();

    // NOTE: Make accessible with short name
    private interface Expandable extends ExpandableItemConstants { }

    public static abstract class MyBaseViewHolder extends AbstractExpandableItemViewHolder {
        public FrameLayout mContainer;

        public MyBaseViewHolder(View v) {
            super(v);
            mContainer = v.findViewById(R.id.container);
        }
    }

    public static class MyGroupViewHolder extends ExpandableNavigationAdapter.MyBaseViewHolder {
        public ExpandableItemIndicator mIndicator;
        public TextView menuGroupTitle;

        public MyGroupViewHolder(View v) {
            super(v);
            mIndicator = v.findViewById(R.id.indicator_menu);
            menuGroupTitle = v.findViewById(R.id.text_menu_group_title);
        }
    }

    public static class MyChildViewHolder extends ExpandableNavigationAdapter.MyBaseViewHolder {
        public TextView menuChildTitle, menuChildPrice;
        public CheckBox cbIsChecked;
        public RelativeLayout rlChild;

        public MyChildViewHolder(View v) {
            super(v);
            menuChildTitle = v.findViewById(R.id.text_menu_child_title);
            menuChildPrice = v.findViewById(R.id.text_menu_child_price);
            cbIsChecked = v.findViewById(R.id.check_box_menu_child);
            rlChild = v.findViewById(R.id.rlChildContent);
        }
    }


    public ExpandableMenuAdapter(Context context, RecyclerViewExpandableItemManager expandableManager, List<Pair<MenuGroupModel, List<MenuChildModel>>> data){
        this.mContext = context;
        this.data = data;
        this.mExpandableManager = expandableManager;
        setHasStableIds(true);
    }

    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return data.get(groupPosition).second.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return Integer.parseInt(data.get(groupPosition).first.getMenuGroupId());
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return Integer.parseInt(data.get(groupPosition).second.get(childPosition).getMenuChildId());
    }

    @Override
    public int getGroupItemViewType(int groupPosition) {
        return 0;
    }

    @Override
    public int getChildItemViewType(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public MyGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_menu_group_item, parent, false);
        return new MyGroupViewHolder(v);

    }

    @Override
    public MyChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_menu_child_item, parent, false);
        return new MyChildViewHolder(v);
    }

    @Override
    public void onBindGroupViewHolder(final MyGroupViewHolder holder, int groupPosition, int viewType) {
        // group item
        final MenuGroupModel menuGroupModel = data.get(groupPosition).first;
        holder.menuGroupTitle.setText(menuGroupModel.getMenuGroupTitle());
        // mark as clickable
        holder.itemView.setClickable(true);
        // set background resource (target view ID: container)
        final int expandState = holder.getExpandStateFlags();

        if ((expandState & ExpandableItemConstants.STATE_FLAG_IS_UPDATED) != 0) {
            boolean isExpanded;
            boolean animateIndicator = ((expandState & Expandable.STATE_FLAG_HAS_EXPANDED_STATE_CHANGED) != 0);

            if ((expandState & Expandable.STATE_FLAG_IS_EXPANDED) != 0) {
                isExpanded = true;
            } else {
                isExpanded = false;
            }

            holder.mIndicator.setExpandedState(isExpanded, animateIndicator);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindChildViewHolder(final MyChildViewHolder holder, final int groupPosition, final int childPosition, int viewType) {
        //child item
        final MenuChildModel childModel = data.get(groupPosition).second.get(childPosition);

        holder.menuChildTitle.setText(childModel.getMenuChildTitle());
        holder.menuChildPrice.setText(childModel.getMenuChildPrice());
        if(childModel.isChecked()){
            holder.cbIsChecked.setChecked(true);
        } else {
            holder.cbIsChecked.setChecked(false);
        }

        if(childModel.getChildPosition().equals("first or middle")){
            holder.rlChild.setBackground(ContextCompat.getDrawable(mContext, R.drawable.first_or_middle_menu_item));
        } else if(childModel.getChildPosition().equals("last")){
            holder.rlChild.setBackground(ContextCompat.getDrawable(mContext, R.drawable.last_menu_item));
        }

        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(childModel.isChecked()){
                    holder.cbIsChecked.setChecked(false);
                    data.get(groupPosition).second.get(childPosition).setChecked(false);
                } else {
                    holder.cbIsChecked.setChecked(true);
                    data.get(groupPosition).second.get(childPosition).setChecked(true);
                }
            }
        });
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(MyGroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        return true;
    }
}
