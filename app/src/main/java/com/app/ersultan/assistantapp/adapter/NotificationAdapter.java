package com.app.ersultan.assistantapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.ersultan.assistantapp.R;
import com.app.ersultan.assistantapp.model.NotificationModel;

import java.util.List;

/**
 * Created by ersultan on 11/2/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>{

    public static final String TAG = NotificationAdapter.class.getSimpleName();
    private Context context;
    private List<NotificationModel> notificationModelList;

    public NotificationAdapter(Context context, List<NotificationModel> notificationModels){
        this.context = context;
        this.notificationModelList = notificationModels;
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notification, parent, false);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        NotificationModel notificationModel = notificationModelList.get(position);
        holder.tvTitle.setText(notificationModel.getTitle());
        holder.tvDate.setText(notificationModel.getDate());
        holder.tvDescription.setText(notificationModel.getDescription());
    }

    @Override
    public int getItemCount() {
        return notificationModelList.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvDate, tvDescription;

        public NotificationViewHolder(View itemView){
            super(itemView);
            tvTitle = itemView.findViewById(R.id.text_notification_title);
            tvDate = itemView.findViewById(R.id.text_notification_date);
            tvDescription = itemView.findViewById(R.id.text_notification_description);
        }
    }
}
